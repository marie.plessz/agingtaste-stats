qui {

		/*
		Defines macros that make the project portable

		Change the path in the macro "projet"

		Run (do) this file once before starting to work on the project.

		Or, more elaborate, but saves time if you work on the project on a regular basis : 

		- find the file called "profile.do"
		- paste this line : replace the path by the path to this dofile on your computer
			global AGT = `"do "C:\Users\ blabla bla \00_AGT_debut.do" "'
		- when you start working on the project : open stata, and type
			$AGT
		this should "do" the dofile 00_AGT_debut and thus, create all the macros that help shorten the paths.

		*/


	// program: 00_AGT_debut.do
	// task: lancer le projet "AGT"
	// project: AGT
	// author: MP le 2019-04-08
	version 15
	clear all
	set linesize 100
	set mem 16g


global projet "C:\Users\user\ownCloud\ART-ConsumptionSociety\AgingTaste-stats"


	global do "$projet/do"
	global source "$projet/data/source" //données originales, ne pas modifier
	global cree "$projet/data/cree" //datasets créés
	global temp "$projet/data/temp" //datasets temporaires, étapes, essais etc
	global res "$projet/resultats"

cd "$projet/"	

	global np "AGT: Aging and changes in taste. Gazel données fév 2015"
}

disp as text "$S_DATE: Bonjour, projet $np" _n "Dossier : $projet"

exit
