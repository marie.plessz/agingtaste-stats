
*****************************
*	Programme mcacontrib	*
*****************************

/* 	Marie Plessz le 24 juin 2014
Description: 
	mcacontrib is a helper to the stata mca command. it computes variable contribution to the
dimensions of the mca as in SPAD, SAS or R package FactoMiner.

	après une MCA,  calcule la contribution des variables aux axes selon les
	formules de Le Roux et Rouanet (Multiple Correspondance analysis)
	et comme dans R et SPAD, en divisant le résultat de stata par la racine carrée
	de la eigenvalue de la dimension correspondante.

A utiliser après 
 	mca [varlist],  method (indicator) normal(princ)
*********************************

Updates
	26/06/2014 :
		- ajouté l'installation des commandes supplémentaires pour les matrices	
		si besoin
		- contributions multipliées par 100 comme dans R et Spad
		- affichage de la matrice NewContrib avec 3 décimales
		- affichage de la contribution moyenne (calculée comme la moyenne des contributions)
		- mis en mode commentaire la boucle qui sauve le nom des var dans une var car ne sert pas.
		- modifié le nom de la matrice qui récupère les anciennes contributions et les noms des colonnes car bug.
*************************************/

program mcacontrib
version 12

*	installer si besoin les fonctions supplémentaires pour matrices
capture which matselrc
if _rc==111 net install   dm79, from(http://www.stata.com/stb/stb56)

*préserver les données en mémoire
preserve

*	Récupérer les valeurs propres et calculer les racines carrées
	*les valeurs propres sont dans e(Ev). 

matrix A=e(Ev)
matrix rcEv=J(1,`e(f)',0)

forval j = 1/ `= e(f)' { 
	mat rcEv[1, `j'] = sqrt(A[1, `j']) /100	
		//je divise par 100 pour avoir les mêmes valeurs que dans R et Spad
} 
svmat2 rcEv, name(matcol)
	// sauve les racines carrées/100 des Ev dans des variables
	
*	récupérer les contributions stata
		//sauver les bonnes colonnes dans de nouvelles variables
forval j = 1/ `= e(f)' { 
	local k = (3*`j') +3
	matselrc e(cGS) Contr`j', col(`k')
	svmat2 Contr`j', /* rname(varname`j') */ names(oldc`j')
} 

/*		// dispensable : sauve aussi le nom des lignes dans une seule var pour pouvoir vérifier
local j=2
while `j'<= `e(f)' { 
	drop varname`j'
	local j=`j'+1
}
*/

* calculer les nouvelles contributions
	//calcul effectué sur les variables
quietly {
	forval j = 1/ `= e(f)' { 
		replace rcEvc`j'=rcEvc`j'[1]
		gen newct`j'=oldc`j' / rcEvc`j'
		label var newct`j' "Contrib `j' nouveau calcul"
	}
}
*	sauver le résultat dans une matrice NewContrib
mkmat   newct* in 1/`=rowsof(e(cGS))', matrix(NewContrib) //rownames(varname1)
	//donner les noms de lignes d'origine à la nouvelle matrice
local subs : rownames e(cGS)
local eqs : roweq e(cGS)
matrix rownames NewContrib = `subs'
matrix roweq NewContrib = `eqs'

	//calculer la contribution moyenne
quietly	{
	sum newct1
}

local Cm r(mean)

*	afficher le résultat
di " - Contributions calculées comme dans SPAD et R(FactoMineR) - "
di "Rappel : utiliser les options method(indicator) normal(principal)"
matlist NewContrib, format(%7.3f)

di _n "Contribution moyenne = "   as res %5.3g `Cm'
di as text  "Résultats sauvés dans la matrice NewContrib"
*	restaurer les données
restore
end
**********************************************
