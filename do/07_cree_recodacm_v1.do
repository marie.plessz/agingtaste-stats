********************************************************************************
* program : cree_05_recode_var_mca
* project : DPS
* author : CD
* date : Mai2014
/* task : préparer les variables alimentaires pour l'analyse factorielle 
*/

********************************************************************************
use "$cree\02_cree_base_wide.dta", clear

* 1# Recoder les variables dont les effetifs après tri à plat sont inférieur à 5%
* Pour 1998
recode viande1998 (1=2), gen (mca_viande_1998)
recode volaille1998(4=3), gen (mca_volaille_1998)
recode feculent1998 ( 1=2), gen (mca_feculent_1998)
recode charcuterie1998 (4=3), gen (mca_charcuterie_1998)
recode poisson1998(4=3), gen (mca_poisson_1998)
recode oeuf1998(4=3), gen (mca_oeuf_1998)
recode legumect1998 (1=2), gen(mca_legume_1998)
recode friture1998(4 3 =2), gen (mca_friture_1998)
recode crudite1998 (1 = 2), gen (mca_crudite_1998)
recode lait1998 (3=2), gen (mca_lait_1998)
recode fruit1998(1=2), gen(mca_fruit_1998)
recode pain1998 ( 1 2 =3), gen (mca_pain_1998)
recode viennoiserie1998(4 3 = 2), gen (mca_viennoiserie_1998)
recode boissonsucree1998 (3 4=2), gen (mca_boissonsucree_1998)
recode matieregrasse1998(4=3), gen (mca_matgrasse_1998)


*Pour 2004 
recode viande2004 (1=2), gen (mca_viande_2004)
recode feculent2004( 1=2), gen (mca_feculent_2004)
recode volaille2004(4=3), gen (mca_volaille_2004)
recode poisson2004(4=3), gen (mca_poisson_2004)
recode oeuf2004(4=3), gen (mca_oeuf_2004)
recode legumect2004 (1=2), gen (mca_legume_2004)
recode friture2004(4 3=2), gen (mca_friture_2004)
recode crudite2004 (1=2), gen ( mca_crudite_2004)
recode lait2004 (3=2), gen (mca_lait_2004)
recode fruit2004 ( 1=2), gen (mca_fruit_2004)
recode charcuterie2004 (4=3), gen (mca_charcuterie_2004)
recode pain2004 (1 2 =3), gen(mca_pain_2004)
recode viennoiserie2004(4 3 =2), gen (mca_viennoiserie_2004)
recode boissonsucree2004 (3 4=2), gen (mca_boissonsucree_2004)
recode sucre2004 (4=3), gen (mca_sucre_2004)
recode matieregrasse2004(4=3), gen (mca_matgrasse_2004)

* Pour 2009
recode viande2009 (1=2) , gen (mca_viande_2009) 
recode volaille2009(4=3), gen (mca_volaille_2009)
recode charcuterie2009 (4=3), gen (mca_charcuterie_2009)
recode poisson2009(4=3), gen (mca_poisson_2009)
recode oeuf2009(4=3), gen (mca_oeuf_2009)
recode friture2009(4 3 =2), gen (mca_friture_2009)
recode feculent2009( 1=2 ), gen (mca_feculent_2009)
recode legumect2009( 1=2), gen(mca_legume_2009)
recode crudite2009 (1=2), gen(mca_crudite_2009)
recode lait2009 (3=2), gen(mca_lait_2009)
recode fruit2009 (1=2), gen(mca_fruit_2009)
recode pain2009 (1 2=3), gen(mca_pain_2009)
recode viennoiserie2009(4 3=2), gen (mca_viennoiserie_2009)
recode sucre2009 (4 = 3 ), gen( mca_sucre_2009)
recode boissonsucree2009 (3 4=2), gen (mca_boissonsucree_2009)
recode matieregrasse2009(4=3), gen (mca_matgrasse_2009)


* Pour 2014 

recode viande2014(1=2) , gen (mca_viande_2014) 
recode volaille2014(4=3), gen (mca_volaille_2014)
recode charcuterie2014 (4=3), gen (mca_charcuterie_2014)
recode poisson2014(4=3), gen (mca_poisson_2014)
recode oeuf2014(4=3), gen (mca_oeuf_2014)
recode friture2014(4 3 =2), gen (mca_friture_2014)
recode feculent2014( 1=2 ), gen (mca_feculent_2014)
recode legumect2014( 1=2), gen(mca_legume_2014)
recode crudite2014 (1=2), gen(mca_crudite_2014)
recode lait2014 (3=2), gen(mca_lait_2014)
recode fruit2014 (1=2), gen(mca_fruit_2014)
recode pain2014 (1 2=3), gen(mca_pain_2014)
recode viennoiserie2014(4 3=2), gen (mca_viennoiserie_2014)
recode sucre2014 (4 = 3 ), gen( mca_sucre_2014)
recode boissonsucree2014 (3 4=2), gen (mca_boissonsucree_2014)
recode matieregrasse2014(4 9 =3), gen (mca_matgrasse_2014)
recode cafe2014 (14 25 = .), gen(mca_cafe_2014)

**** Creation des variables mca_* pour celles non recodées
gen mca_light_1998 = light1998
gen mca_vin_1998=vin1998
gen mca_fromage_1998=fromage1998
gen mca_dessert_1998=dessert1998
gen mca_laitage_1998=laitage1998
gen mca_cafe_1998=cafe1998
gen mca_sucre_1998 =sucre1998

gen mca_light_2004=light2004
gen mca_vin_2004=vin2004
gen mca_fromage_2004=fromage2004
gen mca_dessert_2004=dessert2004
gen mca_laitage_2004=laitage2004
gen mca_cafe_2004=cafe2004

gen mca_light_2009 = light2009
gen mca_vin_2009=vin2009
gen mca_fromage_2009=fromage2009
gen mca_dessert_2009=dessert2009
gen mca_laitage_2009=laitage2009
gen mca_cafe_2009=cafe2009

gen mca_light_2014 = light2014
gen mca_vin_2014=vin2014
gen mca_fromage_2014=fromage2014
gen mca_dessert_2014=dessert2014
gen mca_laitage_2014=laitage2014


* Creation des labels pour les variables mca

label define matgrasse 1"Beurre" 2"Huile" 3"Margarine" 
label value mca_matgrasse_1998 mca_matgrasse_2004 mca_matgrasse_2009 mca_matgrasse_2014 matgrasse

label define cafe 0"Cafe0"1"Cafe1"2"Cafe2", replace
label value mca_cafe_1998 mca_cafe_2004 mca_cafe_2009 mca_cafe_2014 cafe

label define viande 1"Viande0" 2"Viande1"3"Viande2" 4"Viande3"
label value mca_viande_1998 mca_viande_2004 mca_viande_2009 mca_viande_2014 viande

label define volaille 1"Volaille0" 2"Volaille1" 3"Volaille2" 4"Volaille3"
label value mca_volaille_1998 mca_volaille_2004  mca_volaille_2009 mca_volaille_2014 volaille

label define charcuterie 1"Charcuterie0"2"Charcuterie1"3"Charcuterie2" 4"Charcuterie3"
label value mca_charcuterie_1998 mca_charcuterie_2004 mca_charcuterie_2009 mca_charcuterie_2014 charcuterie

label define poisson 1"Poisson0" 2"Poisson1"3"Poisson2" 4"Poisson3"
label value mca_poisson_1998 mca_poisson_2004 mca_poisson_2009 mca_poisson_2014 poisson

label define oeuf 1"Oeuf0"2"Oeuf1" 3"Oeuf2" 4"Oeuf3"
label value mca_oeuf_1998 mca_oeuf_2004 mca_oeuf_2009 mca_oeuf_2014 oeuf

label define friture 1"Friture0" 2"Friture1"3"Friture2"4"Friture3" 
label value mca_friture_1998 mca_friture_2004 mca_friture_2009 mca_friture_2014 friture

label define feculent 1"Feculent0" 2"Feculent1" 3"Feculent2" 4"Feculent3"
label value mca_feculent_1998 mca_feculent_2004 mca_feculent_2009  mca_feculent_2014 feculent

label define legume 1"Legume0" 2"Legume1" 3"Legume2" 4"Legume3"
label value mca_legume_1998 mca_legume_2004 mca_legume_2009 mca_legume_2014 legume

label define crudite 1"Crudite0" 2"Crudite1" 3"Crudite2" 4"Crudite3"
label value mca_crudite_1998 mca_crudite_2004 mca_crudite_2009 mca_crudite_2014 crudite

label define lait 1 "Lait0" 2"Lait1" 3"Lait2" 4"Lait3"
label value mca_lait_1998 mca_lait_2004 mca_lait_2009 mca_lait_2014 lait

label define laitage 1"Laitage0" 2"Laitage1" 3"Laitage2" 4"Laitage3"
label value mca_laitage_1998 mca_laitage_2004 mca_laitage_2009 mca_laitage_2014 laitage

label define fromage 1"Fromage0" 2"Fromage1" 3"Fromage2" 4"Fromage3"
label value mca_fromage_1998 mca_fromage_2004 mca_fromage_2009 mca_fromage_2014 fromage

label define dessert 1"Dessert0" 2"Dessert1" 3"Dessert2" 4"Dessert3"
label value mca_dessert_1998 mca_dessert_2004 mca_dessert_2009 mca_dessert_2014 dessert

label define fruit 1"Fruit0" 2"Fruit1" 3"Fruit2"4"Fruit3"
label value mca_fruit_1998 mca_fruit_2004 mca_fruit_2009 mca_fruit_2014 fruit

label define pain 1"Pain0" 2"Pain1" 3"Pain2"4"Pain3"
label value mca_pain_1998 mca_pain_2004 mca_pain_2009 mca_pain_2014 pain

label define vien 1"Viennois0" 2"Viennois1" 3"Viennois2" 4"Viennois3"
label value mca_viennoiserie_1998 mca_viennoiserie_2004 mca_viennoiserie_2009 mca_viennoiserie_2014 vien

label define sucre2 1"Sucre0" 2"Sucre1" 3"Sucre2" 4"Sucre3"
label value mca_sucre_1998 mca_sucre_2004 mca_sucre_2009 mca_sucre_2014 sucre2

label define light2 0"Ligth0" 1"Light1" 2"Light2"
label value mca_light_1998 mca_light_2004 mca_light_2009 mca_light_2014 light2

label define boisson2 1"Boissucree0" 2"Boissucree1"
label value mca_boissonsucree_1998 mca_boissonsucree_2004 mca_boissonsucree_2009 mca_boissonsucree_2014 boisson2

label define vin 1 "Vin0" 2"Vin1" 3"Vin2" 
label value mca_vin_1998 mca_vin_2004 mca_vin_2009 mca_vin_2014 vin


save "$cree\02_cree_base_wide_acm.dta", replace
