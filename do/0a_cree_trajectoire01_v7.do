********************************************************************************
*	program:	cree_trajectoire01_v7.do
*	project:	RL
*	author:		MP
*	date: 		04/11/2013
*   Adaptation du fichier de MP par CD le 20/05/2015 pour intégration des données 2014 .
/*	task:
prépare ensemble des var dépendant du temps
prépare aussi les var relevées au moment de la retraite
corrige codage date remplissage AQ : aqdate est manquant si pas AQ
corrige info POIDS : manifestement quelques erreurs dans codage. 
supprimé les vars en J
l'info est stockée dans yimc, ytsatis, ytphyfa ytnerfa et nenftot
==>fichier trajectoire01.dta

NB : toutes les var recueillies CHAQUE ANNEE ont un nom qui commence par Y
	 toutes les vars recueillies au MOMENT de la RETRAITE ont un nom qui commence par J
le 12-12-2013 : corrigé yconj_actif pour que soit manquant si ycouple est manquant

V7 : créé des lignes qui manquaient. la var flagligne le signale
*/
local tag "cree_trajectoire01_v7 mp 2013-11-04"
********************************************************************************

//	#1
//récupérer var pertinentes individu
use "$temp\t_ind01pop.dta", clear
gen aqannee=1989
keep proj_gest_ntt aqannee retdate

sort proj_gest_ntt aqannee

save "$temp\t_dateretraite", replace

use "$source\2_DATA_GPSO_ADM_fev2015.dta", clear
keep proj_gest_ntt gpso_adm_source gpso_adm_nenftot
rename gpso_adm_source aqannee
save "$temp\t_enfants", replace


/*créer les lignes manquantes de façon à ce qu'il y ait autant de lignes chaque année ///
	même si elle ne contient aucune info de plus que celles indépendantes du temps. */
use "$temp\t_01aq", clear
keep proj_gest_ntt suivi_aq_aqannee suivi_aq_aqrep
bys proj_gest_ntt (suivi_aq_aqannee): gen last=suivi_aq_aqannee[_N]
	* la var last contient la dernière année effectivement renseignée dans les AQ

reshape wide suivi_aq_aqrep last, i(proj_gest_ntt) j(suivi_aq_aqannee)
reshape long suivi_aq_aqrep last, i(proj_gest_ntt) j(suivi_aq_aqannee)

gen flag_ligne=suivi_aq_aqrep==.
label var flag_ligne "Ligne créée, aqrep mqt"
label val flag_ligne Lon01
note flag_ligne: pour certains individus je n'avais pas de ligne certaines années dans RL03inclus, ///
	alors qu'ils sont dans la cohorte et respectent mes critères d'inclusions. je crée ces lignes ///
	pour lesquelles toutes les vars sauf celles non dépendantes du temps sont manquantes. `tag'
	
bys proj_gest_ntt (suivi_aq_aqannee) : carryforward(last), replace
gen mark=suivi_aq_aqannee>last
* à cette étape on supprime les lignes créées après que la pers soit sortie de l'enquête.

drop if mark
keep proj_gest_ntt suivi_aq_aqannee flag_ligne
save "$temp\t_lignes", replace

//	#2
//	garder var pertinentes AQ
use "$temp\t_01aq", clear
keep proj_gest_ntt suivi_aq_aqannee aq_gener_datr suivi_aq_aqrep aq_gener_cprof ///
	aq_gener_sitfam aq_gener_nper aq_prof_activ aq_prof_activi aq_retr_actret ///
	aq_sante_etats aq_sante_phyfat aq_sante_nerfat aq_gener_poids ///
	aq_gener_taille aq_prof_tphyfa ///
 aq_prof_tnerfa aq_prof_tsatis aq_retr_retpo1 aq_retr_retpo2 aq_retr_retpo3 ///
 aq_retr_retpo4 aq_retr_ract1 aq_retr_ract2 aq_retr_ract3 aq_retr_ract4 ///
 aq_retr_ract5 aq_retr_rsatis aq_retr_rsante aq_retr_affret aq_gener_nenf 
	
merge 1:1 proj_gest_ntt suivi_aq_aqannee using "$temp\t_lignes" 
drop _merge
 
***** vérif qu'on a bien 1 li /indv
bys proj_gest_ntt ( suivi_aq_aqannee) : gen ok= suivi_aq_aqannee[_n] -suivi_aq_aqannee[_n-1]==1
replace ok=1 if  suivi_aq_aqannee==1989
tab ok
 
gen aqannee = suivi_aq_aqannee

merge m:1 proj_gest_ntt aqannee using "$temp\t_dateretraite"
drop _merge


*	boucher les trous 
*ssc install carryforward
bys proj_gest_ntt (aqannee) : carryforward(retdate), replace


//	#3
//	recodages
* remplissage AQ
clonevar aqrep=suivi_aq_aqrep
label val aqrep Lon01
note aqrep: copie de suivi_aq_rep / `tag'

/*date remplissage est déjà corrigée. quand elle est manquante c que pas d'AQ
gen  aqdate=aq_gener_datr 
gen b=mdy(1,1,aqannee)
format b %d
replace aqdate=b if aqdate==. & aqrep==1
format aqdate %d
label var aqdate "Date remplissage AQ (01/01 si date miss)"
note aqdate: copie de aq_gener_datr. si manquante ALORS QUE réponse à l'AQ (aqrep==1) ///
	, 01/01 de l'année, comme dans doc PRCP  / `tag'
*/

clonevar aqdate=aq_gener_datr
note aqdate: copie de aq_gener_datr. si manquante c'est parce que pas d'AQ  / `tag'
	
local vardrop "suivi_aq_aqannee aq_gener_datr suivi_aq_aqrep ok"

recode aq_gener_sitfam (1 4/6=0)(2 3=1) (7 = .) , gen(ycouple)
label value ycouple Lon01
label var ycouple "En couple (AQ)"
note ycouple: d'après l'auto-questionnaire. si on utilise conj_actif cette var est ///
	inutile / `tag'

gen yconj_actif = aq_gener_cprof 
replace yconj_actif=0 if ycouple==0
replace yconj_actif=. if ycouple==.
label var yconj_actif "Conjoint en emploi (AQ)"
label def yconj_actif 0 "0_Pas de conjoint" 1 "Cjt travaille" ///
	2 "Cjt hors emploi", modify
label val yconj_actif yconj_actif
note yconj_actif: d'après l'auto-questionnaire / `tag'

local vardrop " `vardrop' aq_gener_sitfam aq_gener_cprof"

gen ynper=aq_gener_nper
recode ynper (0=.)(8/99=7)
label var ynper "Taille ménage AQ (max 7)"
label def ynper 7 "7 et plus"
label val ynper ynper
note ynper: `tag'

gen ynenf = aq_gener_nenf
recode ynenf (99 = .) (6/20 = 6)
label var ynenf "Nb d'enfants dans foyer (max 6)"
label def ynenf 6 "6 ou plus"
label val ynenf ynenf
note ynenf : `tag'


gen yactif= aq_prof_activ if aqannee<2003
replace yactif=aq_prof_activi  if aqannee>2002
recode yactif (2=5)
replace yactif=4 if aq_retr_actret==1 & inrange(suivi_aq_aqannee, 2000, 2002)
label var yactif "Situation professionnelle (AQ)"
label def yactif 1 "En emploi"  3 "Retraite préretraite" ///
	4 "Ret. en activité" 5 "Autre inactif (invalidité)", modify
label value yactif yactif
note yactif: codé d'après AQ. Inactif autres raisons recodé avec maladie etc. ///
	Beaucoup de valeurs manquantes / `tag'

local vardrop " `vardrop'  aq_prof_activ aq_prof_activi aq_retr_actret aq_gener_nper aq_gener_nenf"

clonevar yetats=aq_sante_etats
label def yetats 1 "Très bon" 8 "Très mauvais"
label val yetats yetats

clonevar yphyfat=aq_sante_phyfat
clonevar ynerfat=aq_sante_nerfat
label def Lfat 1 "Pas du tout" 8 "Très"
label val yphyfat ynerfat Lfat


//	#4 
//	imc

* corriger les cas où il est évident que la pers a mis un 1 de trop devant son poids.
	bys proj_gest_ntt (aqannee): egen m=mean(aq_gener_poids)
	gen g= aq_gener_poids - m

	gen poidscorr=aq_gener_poids
	replace poidscorr=aq_gener_poids-100 if aq_gener_poids>134 & g>60 & aq_gener_poids<.
	gen ipoidscorr=poidscorr!=aq_gener_poids

	*corriger quelques poids trop faibles manifestement mal codés
	replace poidscorr=75 if  proj_gest_ntt==48116848  & aqannee== 1996
	replace poidscorr=93 if  proj_gest_ntt==48104245  & aqannee== 1994
	replace poidscorr=95 if proj_gest_ntt==48110079  & aqannee== 1990
	replace poidscorr=95 if proj_gest_ntt==48103101  & aqannee== 1996
	replace poidscorr=95 if proj_gest_ntt==48113413  & aqannee== 1994
	replace poidscorr=. if proj_gest_ntt==48103112  & aqannee== 1996
	replace poidscorr=. if proj_gest_ntt==48106968  & aqannee== 1993
	* non corrigé car trend de poids fortement ascendant: 48117371          2011           

		*vérification
	bys proj_gest_ntt (aqannee): egen m2=mean(poidscorr)
	gen g2=poidscorr- m2  
	
	*var° % précédente ou suivante
	bys proj_gest_ntt (aqannee): gen  dlpoids = poidscorr - poidscorr[_n-1]
	bys proj_gest_ntt (aqannee): gen  dfpoids = poidscorr - poidscorr[_n+1]
	gen varl=(dlpoids<-25 | dlpoids>25) & dlpoids<.
	gen varf=(dfpoids<-25 | dfpoids>25) & dfpoids<.
	gen varm=(g2< -20 | g2>20) & g2<.
	gen var2=varl+varf+varm
	recode var2 (0 1=0)(2 3=1)
	replace poidscorr=. if var2==1
	
	label var poidscorr "Poids AQ corrigé"
	note poidscorr: corrigé les valeurs aberrantes: 1/ si plus de 80kg de ///
		+ que le poids moyen et poids>134kg; 2/si moins de40kg q poids moyen ///
		avec vérification au cas par cas; 3/si au moins 2 des var (diff%obs précédent; ///
		diff%obs suivante; diff%moy) est supérieure en valeur absolue à 25 (20 pour la moy) ///
		/ `tag'	
drop m
	
	*taille	
/*il y a des trous (la question n'est posée que de temps en temps tant 
que les gtens sont jeunes (ils ne changent pas de taille)
Je fais comme dans la publi µDugravot et al  2010 (do socioeconomic factors
shape weight trajectories?) : je prends taille 1990
j'ai vérifié que les tailles de 1990 étaient cohérentes
*/
bys proj_gest_ntt (aqannee): gen taille90=aq_gener_taille[2]

label var taille90 "Taille AQ 1990"
note taille90: la taille n'a été demandée qu'en 1990, puis dep 1997 avec des trous ///
	je prends la valeur de 1990 pour tout le monde (cf Dugravot et al 2010) / `tag'
	
*imc
 gen yimc= poidscorr /(taille90/100)^2 
 label var yimc "IMC (AQ)"
 note yimc: J'utilise la taille en 1990 pour toutes les obs et le poids corrigé /`tag'
 
*verif imc
bys proj_gest_ntt (aqannee): egen m3=mean(yimc)
	gen g3= yimc - m3

local vardrop "`vardrop' ipoidscorr g m2 g g2 g3 m3 dlpoids dfpoids varl varf varl varm var2"
local vardrop "`vardrop' aq_gener_poids aq_gener_taille aq_sante_etats "
local vardrop "`vardrop' aq_sante_phyfat aq_sante_nerfat "

//	#4
//	coder infos % travail, juste avant retraite
/*
gen av=aq_gener_datr<retdate & aq_gener_datr<.
bys proj_gest_ntt  ( aqannee): egen jpb=sum(av)
recode jpb(0=1)(else=0)

foreach X in tsatis tnerfa tphyfa {
	bys proj_gest_ntt  (av aqannee): gen j`X'=aq_prof_`X'[_N] 
	bys proj_gest_ntt  (av aqannee): replace j`X'=aq_prof_`X'[_N-1] if aq_prof_`X'[_N] ==.
	bys proj_gest_ntt  (av aqannee): replace j`X'=. if av[_N]!=1
	label val j`X' Lfat
	note j`X': valeur de aq_prof_`X' dans dernier AQ avant retraite ou 1 an avant / `tag'
	local vardrop "`vardrop' aq_prof_`X' "
}


label var jtsatis "Satisfait de votre travail? 1 an avant retraite"
label var jtnerfa "Travail nerveust fatigant? 1 an avant retraite"
label var jtphyfa "Travail physiqt fatigant? 1 an avant retraite"
*/

foreach X in tsatis tnerfa tphyfa {
	gen y`X'=aq_prof_`X'
	label val y`X' Lfat
	note y`X': copie de aq_prof_`X' / `tag'
	local vardrop "`vardrop' aq_prof_`X' "
}

label var ytsatis "Satisfait de votre travail?"
label var ytnerfa "Travail nerveust fatigant?"
label var ytphyfa "Travail physiqt fatigant?"

//	5 
//	opinion sur retraite
/*	je renonce à code ces questions car je ne comprends pas les filtres.
il y a tellement de NR que c'est pas très intéressant.
je laisse tomber pour le moment.
*/

/*

//	6
//	nombre d'enfants: last obs avant retraite NENFTOT.

Note du 2021-07-05 : nenftot n'est pas une variable dépendante du temps. j'essaie de la supprimer car ça crée des problèmes.


merge 1:1 proj_gest_ntt aqannee using   "$temp\t_enfants"
drop _merge // il manque des obs dans gpso car pas extraction ts les ans

/*gen m=gpso_adm_nenftot!=.
bys proj_gest_ntt  (m av aqannee): gen jnenftot=gpso_adm_nenftot[_N] 
recode jnenftot (5/max=5)
label def jnenftot 5 "5 et +"
label val jnenftot nenftot
label var jnenftot "Nombre enfants eus (last avant retraite)"	
note jnenftot: dernière valeur de gpso_adm_nenftot renseignée avant date ///
	passage en retraite / `tag'

gen jparent= jnenftot!=0
label var jparent "A eu des enfants (GPSO)"
label val jparent Lon01
note jparent : dans les données du gpso_adm, dernière observation valide de ///
	gpso_adm_nenftot avant retraite est >0 / `tag'
drop m
*/

***je renonce à coder cette var au moment de la retraite. je prends la valeur max enregistrée
gen m= gpso_adm_nenftot<.
bys proj_gest_ntt  ( m gpso_adm_nenftot): gen nenftot=gpso_adm_nenftot[_N]
recode nenftot (4/max=4)
label def nenftot 4 "4 et +"
label val nenftot nenftot
label var nenftot "Nombre d'enfants (GPSO)"
note nenftot: valeur maximale non manquante de nenftot dans gpso / `tag'
drop m
local vardrop "`vardrop' gpso_adm_nenftot  "

*/

//	7
//	imc au moment de la retraite
*je renonce à cette var pour l'instant. je garde l'imc recueilli chaque année

/*gen m=yimc!=.
bys proj_gest_ntt  ( av aqannee): gen jimc=yimc[_N] 
bys proj_gest_ntt  (av aqannee): replace jimc=. if av[_N]!=1
label var jimc "IMC juste avant retraite"
note jimc : si poids manquant on prend la dernière observation valide / `tag'
drop m

gen jimc25=jimc>=25 & jimc<30
gen jimc30=jimc>=30 & jimc<.
replace jimc25=. if jimc==.
replace jimc30=. if jimc==.
label var jimc25 "IMC 25à 30 avant retraite"
label var jimc30 "Obèse avant retraite"
note  jimc25: jimc entre 25 et 30 / `tag'
note  jimc30: jimc >=30 / `tag'
*/
local vardrop "`vardrop' aq_retr_* "
//	8
//	nettoyer, sauver
*finir le nettoyage
dis "`vardrop'"

drop `vardrop' 
order proj_gest_ntt aqannee aqdate y*
sort proj_gest_ntt aqannee
compress
save "$cree\trajectoire01.dta", replace
