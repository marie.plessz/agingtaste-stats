********************************************************************************
*	program:	cree_invalidite_v1.do
*	project:	ALV
*	author:		MP
*	date: 		12 nov 2013
* Adaptation du dofile de MP le 20 Mai 2015 par CD pour intégration des données 2014
/*	task:
préparer date sortie effective du travail pour les pers qui ont eu longue maladie
invalidité ou maladie pro.
*/
local tag "cree_invalidite_v1.do cd 20mai2015"
********************************************************************************
use "$source\DATA_LM_INV_MP_NL", clear
save "$temp\t_invalidite_v1", replace

gen double invdate=date1_lm_gpso
replace  invdate= date1_lm_sgmc if date1_lm_sgmc<invdate
replace  invdate= date1_mp_sgmc if date1_mp_sgmc<invdate
replace  invdate= date1_inv_gpso if date1_inv_gpso<invdate

format invdate %d

label var invdate "Date 1° invalidité/LM/MP"
note invdate: première date parmi mise en invalidite, mise en longue ///
	maladie, maladie professionnelle sans retour au travail avant retraite `tag'

clonevar invaliditedate=date1_inv_gpso
note invaliditedate : date mise en invalidité (GPSO) clone de date1_inv_gpso `tag'
	
gen inv=date1_inv_gpso <.
gen mp=	date1_mp_sgmc<. | date1_lm_gpso<.
gen lm=date1_lm_sgmc <.

label var inv "A été en invalidité"
label var mp "a été en maladie professionnelle"
label var lm "a été en longue maladie"
label val inv mp lm Lon01
	
* indicatrice a été en invalidité (339), longue maladie (1143) ou maladie pro (51 indv)
/* ces catégories se recoupent
*/
gen invlmmp= (inv==1 |lm==1 | mp==1)
label var invlmmp "A eu invalidite/LM/MP"
label val invlmmp Lon01
note invlmmp:  a été en invalidité, longue maladie ou maladie pro au moins 1 fois. `tag'
drop inv mp lm		
	
*nettoyer sortir
drop date1_inv_gpso date1_lm_gpso date1_lm_sgmc date1_mp_sgmc
compress

save "$temp\t_invalidite_v1", replace
