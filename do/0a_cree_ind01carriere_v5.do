/*********************************************************************************
program: 	cree_ind01carriere_v5
project: 	ALV
author: 	CD le 20 Mai 2015
task: 	
	extraire de GPSO_CRES_NL la position pro à l'entrée à EDF 
	et au moment de la retraite
	ajout CSP 1 chiffre
v5 le 13 dec 2013 : le gf à 35 ans est déjà codé par l'équipe gazel.
je n'ai pas besoin de le faire moi-même

sortie: 	carriere01.dta
********************************************************************************/
local tag "cree_ind01carriere_v5.do CD 20Mai2015"

// #1
// réunir infos sur carrière et sur âge
use "$source\DATA_GPSO35.dta", clear
rename ntt proj_gest_ntt
sort proj_gest_ntt
save "$temp\t_gpso35", replace

use "$source\2_DATA_GPSO_CRES_fev2015", clear
save "$temp\t_gpso_cres.dta", replace

use "$temp\t_gpso_cres.dta", clear
merge m:1 proj_gest_ntt using "$temp\t_ind01pop.dta"
cap drop _merge

merge m:1 proj_gest_ntt using "$temp\t_gpso35"
cap drop _merge

// #2
// créer position professionnelle entrée, sortie, mobilité
*entrée
generate double datmv = date(gpso_car_datmv, "DMYmh")
format datmv %d
bys proj_gest_ntt (datmv) :gen gf1_entree=gpso_car_gf1[1]
bys proj_gest_ntt (datmv) :gen pcs_entree=gpso_car_insee[1]

bys proj_gest_ntt (datmv) :replace gf1_entree=gpso_car_gf1[2] if gpso_car_gf1[1]==.
bys proj_gest_ntt (datmv) :replace pcs_entree=gpso_car_insee[2] if gpso_car_insee[1]==""

replace pcs_entree=substr(pcs_entree, 1, 1)
destring pcs_entree, replace
recode pcs_entree (2 9=.)
label var pcs_entree "PCS entrée EDF (GPSO)"
note pcs_entree: 1° chiffre de la pcs à 4 chiffres. Si 1° ligne manquante je prends ligne 2 / `tag'

*sortie
bys proj_gest_ntt (datmv) : gen jgf1=gpso_car_gf1[_N]
bys proj_gest_ntt (datmv) :gen jpcs=gpso_car_insee[_N]
replace jpcs=substr(jpcs, 1, 1)
destring jpcs, replace
recode jpcs (2 9=.)
label var jpcs "PCS sortie EDF (GPSO)"
note jpcs: 1° chiffre de la pcs à 4 chiffres / `tag'

*mobilité
bys proj_gest_ntt (datmv) :gen gf1_mob=gpso_car_gf1[_N] - gf1_entree

label var gf1_entree "Groupe pro entrée EDF (GPSO)"
label var jgf1 "Groupe pro sortie EDF (GPSO)"
label var gf1_mob "Différence gpe pro entrée-sortie"

label def Lgf1 1 "Exécution" 2 "Maîtrise" 3 "Cadre", modify
label value gf1_entree Lgf1
label value jgf1 Lgf1

label def Lpcs ///
	1 "Agriculteur, indépendant" ///
	3 "Cadre" ///
	4 "Prof. intermédiaire" ///
	5 "Employé" ///
	6 "Ouvrier" ///
	9 "NR"	///
	11 "Retraité" ///
	12 "Hors emploi" , modify
label value pcs_entree jpcs Lpcs

note gf1_entree  : groupes fonctionnels définis par EDF-GDF. Si 1° ligne manquante je prends ligne 2 / `tag'
note jgf1: groupes fonctionnels définis par EDF-GDF / `tag'
note gf1_mob: différence jgf1 - gf1_entree / `tag'
// #3
// créer position pro à 35 ans : 
	// gf1_35= la var crée par l'équipe GAZEL
gen gf1_35 =gpso_gf35
	//si indv pas EDF à 35 ans, je prends la position à l'entrée
replace gf1_35=gf1_entree if gpso_gf35==.
	
label var gf1_35 "Groupe fonctionnel à 35 ans (GPSO)"
label value gf1_35  Lgf1 
note gf1_35: variable créée par l'équipe gazel. si pas EDF à 35 ans, position à l'entrée. ///
	si celle-ci est manquante, 2ème ligne du fichier cres / `tag'

	// idem pour pcs
gen pcs_35=substr(gpso_insee35, 1, 1)
replace pcs_35=string(pcs_entree) if gpso_insee35==""
destring pcs_35, replace
	
label var pcs_35 "PCS à 35 ans (GPSO)"
label value pcs_35  Lpcs
note pcs_35: variable créée par l'équipe gazel. si pas EDF à 35 ans, position à l'entrée. ///
	si celle-ci est manquante, 2ème ligne du fichier cres / `tag'

	// si pcs Existe alors que gf1 est manquant, j'utilise l'info : 
replace gf1_35=1 if gf1_35==. & pcs_35==6

// #4
// nettoyer, sauver
bys proj_gest_ntt : gen l=_n
keep if l==1
keep proj_gest_ntt  gf1_mob gf1_35 gf1_entree jgf1 ///
	pcs_entree pcs_35 jpcs

desc
compress
sort proj_gest_ntt
save "$temp\t_ind01carriere", replace
