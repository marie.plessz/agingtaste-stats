*11_AGT_statsdescs

********************************************************************************
*	program:  11_AGT_statsdescs
*	project: AGT
*	author: MP
* 	descriptive statistics

* Distribution of Baseline and time-dependent variables for the study population

* destribution of Baseline variables for full population and study population



* R1 : isolated individuals out of sample due to attrition (death or exit). study population
*does not change.
********************************************************************************

use "$cree\9_AGT_axes.dta", clear

keep proj_gest_ntt a aqrep femme a98axe* diplom3 yage* generation  couplebl ycouple yenf01

recode yage (45/49 = . ) (50/54=50 "50-54") (55/59 = 55 "55-59") (60/64 = 60 "60-64") (65/69 = 65 "65-69") (70/max = 70 "70-75") , gen(yagecl)

* tableau % de participants vivant avec au moins 1 enfant selon âge, sexe et diplome
table  yagecl  diplom3 , c(mean yenf01) by(femme)

*==> j'ai fait le graphique dans excel.


tabout diplom3  generation couplebl femme if a == 1998   ///
	   using "$res/11_AGT_statsdescs_1.csv"	///
	, replace style(semi)  cell( col) f(1c) npos(col) /*stats(chi2)  */	///
	h1(Descriptive statistics - study population) 
	
	
foreach a in 1998 2004 2009 2014 { 
	tabout  ycouple yenf01 femme if a == `a' ///
	   using "$res/11_AGT_statsdescs_1.csv"	///
	, append style(semi)  cell( col) f(1c) npos(col) /*stats(chi2)  */	///
	h1(Descriptive statistics study population - Timevarying variables - `a') 
	}
	

tab femme if a == 1998

* graphe situation conjugale temps

gen enf100 =  (1 - yenf01) * 100
gen cou100 = (1 - ycouple) * 100

 graph bar enf100 cou100 , over(yagecl) legend(label(1 "No child") label(2 "No partner") ///
	title ("Lives with")) scheme(s1mono) title("Living arrangements over time in Gazel cohort") ///
	ytitle("Percent") blabel(bar,  format(%4.1f) gap(*0.5)  )

graph display
graph export "$res/11_AGT_statsdescs_graph_household.png", replace
graph export "$res/11_AGT_statsdescs_graph_household.ps", replace
graph export "$res/11_AGT_statsdescs_graph_household.tif", replace width(4000)


* stats descs before attrition

use  "$cree\02_cree_base_wide_acm.dta", clear


keep proj_gest_ntt aqrep1998 femme couplebl diplom3 generation ycouple1998 ynenf1998

recode ynenf1998 (0 = 0) (. = .) (else = 1), gen(yenf1998)

label var yenf1998 "Lives with child(ren)"

tabout diplom3  generation couplebl  femme ///
	   using "$res/11_AGT_statsdescs_1.csv"	///
	, append style(semi)  cell( col) f(1c) npos(col) /*stats(chi2)  */	///
	h1(Descriptive statistics 1998 - full sample) 

	