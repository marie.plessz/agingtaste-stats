/*
author : Marie Plessz
Project : Agingtaste - consumption and society. revision

 TASK : check attrition
 
The results of this is now part of the "cree" files. (namely 0a_cree_ind01pop_v3.do et 08_inclus.do).
 
*/

use  "$source\0_DATA_POP_fev2015.dta", clear

count
* 20625

********* nombre de départ d'EDF *********
cap drop a
gen a = substr(suivi_pop_pdfdate, 7, 4)
destring(a), gen(exitedfdate)

count if exitedfdate < 2014

* ==>   98

table exitedfdate 


* tous ont quitté l'entreprise avant 2007

********* demande de ne plus recevoir le questionnaire  **********
count

cap drop a
gen a = substr(suivi_pop_dexdate, 7, 4)
destring(a), gen(dropdate)

count if dropdate < 2014

*   520  ont demandé à quitter la cohorte (+ 23 en 2014)

*drop  if dropdate < 2014

******* nombre de décès avant 2014 *******

count

* ==>   20,105 

cap drop a
gen a = substr(suivi_pop_dcddate, 7, 4)
destring(a), gen(dcdate)

count if dcdate<2014

* ==> décédés avant 2014 :     1,974

count if dcdate == 2014 

* ==> décédés en 2014 :    172 

* drop if dcdate < 2014
 
count

* ==>   18,131


di "Taux d'attrition dû à exit ou décès  : "  %5.2f  (1- (r(N) /20625))*100 " %"
* ==> Taux d'attrition dû à exit ou décès  : 12.09 %

