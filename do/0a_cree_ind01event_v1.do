********************************************************************************
*	program:	cree_ind01event_v1.do
*	project:	ALV
*	author:		CD
*	date: 		20 mai 2015
*   Adaptation du fichier de Marie P. du 18oct 2013 pour insertion des données 2014
/*	task:
décrire l'échantillon sur la base du fichier "individu" 
*/
local tag "cree_ind01event_v1.do cd 20 Mai 2015"
********************************************************************************

//	#1
//	données
use "$source\1_DATA_AQ_EVEN_fev2015.dta", clear
save "$temp\t_ind01event", replace

//	#2
//	codage

gen b=aqannee if  ref_aq_even_lib2eve=="Cessation d’activité ou retraite" ///
	& ref_aq_even_lib1eve=="Conjoint volontaire"

bys proj_gest_ntt: egen ev_retconjoint=sum(b)
replace ev_retconjoint=-1 if ev_retconjoint>2013 & ev_retconjoint<.

label def ev_retconjoint -1 "Plusieurs retraites de conjoints" ///
	0 "Aucune retraite conjoint", modify
label value ev_retconjoint ev_retconjoint


label var ev_retconjoint "Evént retraite conjoint: année"	
note ev_retconjoint: attention information à utiliser en complément de la var ///
codée tous les ans. rien n'oblige les volontaires à déclarer retraite conjoint.  `tag'


/*problème: y a des gens, ils divorcent et après ils se remettent en couple, et après
leur nouveau/elle conjoint(e) passe à la retraite...
c pas des emmerdeurs ça?
bref je laisse tomber temporairement.
*/

/*
j'ai codé les décès de conjoint et divorces	mais il y a des biscouettes dans les data
en particulier certaines années il n'y a pas de ligne pour les divorces/séparation 
ce qui introduit des bizarreries.
ne pas utiliser
*/
*gen ev_fin_union=aqannee if ///
*	(ref_aq_even_lib2eve=="Décès" & ref_aq_even_lib1eve=="Conjoint volontaire") |	 ///
*	(ref_aq_even_lib2eve=="Séparation ou divorce" & ref_aq_even_lib1eve=="Volontaire")


//	#3  nettoyer sauver sortir
bys proj_gest_ntt: keep  if _n==1
keep proj_gest_ntt ev_retconjoint
compress
save "$temp\t_ind01event", replace
