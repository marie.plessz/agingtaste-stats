
local tag "R2check_10b_AGT_modeles_subpop"
cap log close
* log using "$res/`tag'_log.txt", text replace

di "`tag'"
/*
MP le 24/04/2022
check whether wie get different results if we isolate one cohort
*/


* Original data: complete cases	  

use "$cree\9_AGT_axes.dta", clear

keep proj_gest_ntt a aqrep femme a98axe* diplom3 yage* generation ycouple yenf01
	   
*==========		Parameters		===============*	

*** modèle avec âge du début à la fin, à partir de 50 ans (avant, pas assez de points et que des femmes)

*macro contenant les paramètres communs aux graphs
global marginopts " xlabel(-10 "50" -5 "55" 0 "60" 5 "65"  10 "70" 15 "75 y.") xtitle(Age) nodraw  scheme(s1mono)  yline(0, lcolor(gray)) recast(line)  legend(rows(1) subtitle("Education"))"

*macros contenant les paramètres spécifiques à chaque axe
local opt1 "ytitle("Dim.1 Convenience")  ytick(-40(10)20) ylabel(-40(10)20) xtitle("")  "
local opt2 "ytitle("Dim.2 Health")   ytick(-40(10)20) ylabel(-40(10)20) title("") xtitle("") "  // 
local  opt3 "ytitle("Dim.3 Tradition")  ytick(-40(10)20) ylabel(-40(10)20) title("") "

*pour étiquetage des deux colonnes
local sex1 "Women"
local sex0 "Men"
forvalues s = 0/1 {
di "`sex`s''"
}

*===========	Models with only the 1944-1948 birth-cohort	=================*

*modèle et graphes des valeurs prédites
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yageC c.yageC#i.diplom3 i.diplom3 if /// take generation out of predictors
			femme==`s'  & yage>49 &	generation == 1  ///  select birth cohort 1944-48, 
			|| proj_gest_ntt : 

		eststo AX`v'F`s'

		*estat icc
				quietly margins i.diplom3, at((base) _factor yageC = (-10(5)15) )
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot,  title("`sex`s''") ///
			name(gAX`v'F`s', replace)  /// chaque graph est nommé, pas sauvé
			$marginopts `opt`v''
	}
}

* tableau des  résultats 
esttab AX*F*  using "$res/`tag'_Cohort44-48.txt", ///
	title("Changes in eating over age : cohort 1944-1948") ///
	not label mtitles(H1 H2 H3 F1 F2 F3) b(a2) nobaselevels replace tab


*===========	Models with only the 1944-1948 birth-cohort	=================*

*modèle et graphes des valeurs prédites
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yageC c.yageC#i.diplom3 i.diplom3 if /// take generation out of predictors
			femme==`s'  & yage>49 &	generation == 0  ///  select birth cohort 1944-48, 
			|| proj_gest_ntt : 

		eststo AX`v'F`s'

		*estat icc
				quietly margins i.diplom3, at((base) _factor yageC = (-10(5)15) )
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot,  title("`sex`s''") ///
			name(gAX`v'F`s', replace)  /// chaque graph est nommé, pas sauvé
			$marginopts `opt`v''
	}
}


* tableau des  résultats 
esttab AX*F*  using "$res/`tag'_Cohort39-43.txt", ///
	title("Changes in eating over age  : cohort 1939-43") ///
	not label mtitles(H1 H2 H3 F1 F2 F3) b(a2) nobaselevels replace tab

exit

*===========	Models with only ages 55-69	=================*

*modèle et graphes des valeurs prédites
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yageC c.yageC#i.diplom3 i.diplom3 i.generation if /// take generation out of predictors
			femme==`s'  & yage > 54 &	yage < 69 ///  select birth cohort 1944-48, 
			|| proj_gest_ntt : 

		eststo AX`v'F`s'

		*estat icc
				quietly margins i.diplom3, at((base) _factor yageC = (-10(5)15) )
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot,  title("`sex`s''") ///
			name(gAX`v'F`s', replace)  /// chaque graph est nommé, pas sauvé
			$marginopts `opt`v''
	}
}

* tableau des  résultats 
esttab AX*F*  using "$res/`tag'_age55-69.txt", ///
	title("Changes in eating over age: ages 55-69") ///
	not label mtitles(H1 H2 H3 F1 F2 F3) b(a2) nobaselevels replace tab
