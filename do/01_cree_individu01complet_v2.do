********************************************************************************
*	program:  cree_individu01vomplet_v2.do
*	project: RL
*	author: MP
* Adaptation du fichier de MP par CD le 20/05/2015 pour intégration des données 2014. 
/*	task:
Met ensemble les var tirées de pop, de l'AQ et des carrières pour faire le fichier
individu01.dta
*/
local tag "cree_individu01complet_v2 cd 2015-05-20"
********************************************************************************


//	#1
//	crer les "morceaux" tirés de pop, aq et carrières"
label def Lon12 1"Oui" 2 "Non", modify
label def Lon01 0 "Non" 1 "Oui", modify

do "$do\0a_cree_ind01pop_v3.do" 
do "$do\0a_cree_ind01carriere_v5.do" 
do "$do\0a_cree_ind01aq_v5.do" 
do "$do\0a_cree_ind01event_v1.do"

//	#2
//	créer les fichiers à ajouter à individu dans le dofile n° 2 
	// cree_RL01
do "$do\0a_cree_alimentation01_v3"
do "$do\0a_cree_trajectoire01_v7"
do "$do\0a_cree_invalidite_v1"
do "$do\0a_cree_codepostal_v1.do"

*je n'ai pas besoin du bout "retraite" qui a été "fondu" avec "trajectoire"

//#4
//réunir les 3 morceaux
use  "$temp\t_ind01aq", clear
merge 1:1 proj_gest_ntt using "$temp\t_ind01pop"
drop _merge
merge 1:1 proj_gest_ntt using "$temp\t_ind01carriere"
drop _merge
merge 1:1 proj_gest_ntt using "$temp\t_ind01event"
drop _merge
//	#5
//	Nettoyage
rename suivi_aq_aqannee aqannee

compress
order proj_gest_ntt	 ///
	aqannee ///
	retdate	 ///
	annais	 ///
	datnais ///
	neap45 ///
	femme	 ///
	diplom	 ///
	statutcjtbl ///
	statutpere ///
	conj_pro_89	 ///
	ev_retconjoint ///
	fumeur89 	///
	nbhab_92	 ///
	revenu_89	 ///
	revenu_02	 ///
	revpc_89	 ///
	revpc_02	 ///
	proouv	 ///
	proper	 ///
	jage	 ///
	gf1_entree	 ///
	jgf1	 ///
	gf1_mob	 ///
	gf1_35	 ///
	pcs_entree ///
	pcs_35 ///
	jpcs ///
	sortieraison	 ///
	sortiedate	///
	
 notes drop _dta
 notes _dta: Fichier contenant toutes les caractéristiques individuelles ///
	non dépendantes du temps. 1 ligne par indv. `tag'
save "$cree\individu01.dta", replace
