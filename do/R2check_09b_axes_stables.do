/*
Author: MP
projet : AGT
TASK : check  the stability of the MCA performed in 1998 over time : 
would individuals had different coordinates if we  made a new ACM each year?

We run one MCA aper year and examine :
- whether the same variables contribute to the axes
-whether the individual coordinates derived from these MCAs (in which individuals 
	are active every year) correlate with the coordinates derived from the 1998 
	MCA (in which individuals are supplementary in 2004-2014).
*/


***** Check stabilité des axes ****

use "$cree\9_AGT_axes.dta", clear


keep proj_gest_ntt a a98*
rename a98* orig98*

merge 1:1 proj_gest_ntt a using "$cree\R2check_09_AGT_axes.dta",  keepusing(a98*)  
rename a98* avail98*

table _merge
/*

    Result                           # of obs.
    -----------------------------------------
    not matched                        15,772
        from master                         0  (_merge==1)
        from using                     15,772  (_merge==2)

    matched                            41,700  (_merge==3)
    -----------------------------------------
*/

drop _merge

reshape wide orig98* avail98* , i(proj_gest_ntt) j(a)



* Corrélation entre les positions des individus dans l'ACM réalisée en 1998, 
*et la position qu'ils auraient eu si on avait fait une nouvelle ACM chaque année


* les vars a98* contiennent les positions des individus dans l'ACM de 1998 : individus actifs en 1998 et supplémentaires ensuite.

cap erase "$res/R2check09_MCA_correlations-axes.csv"
tempname myfile
file open `myfile' using "$res/R2check09_MCA_correlations-axes.csv", write
file write  `myfile' _n "Axis; Year; Coeff"
forvalues i = 1/3 { 
	foreach an of numlist 1998 2004 2009 2014 { 
	corr  orig98axe`i'`an' avail98axe`i'`an'
	local coefcor = round( r(rho) , .001)
	file write `myfile' _n " `i' ; `an'  ;  `coefcor' "
	}
}
file close `myfile'

exit

