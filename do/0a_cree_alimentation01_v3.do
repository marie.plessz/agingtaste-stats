********************************************************************************
*	program:  cree_alimentation01_v3.do
*	project: ALV
*	author: MP
*	date: 	19/11/2013
* Adaptation du fichier de MP (cree_legume01_v3) par CD le 20 mai 2015 pour intégration des données 2014 .
/*	task: créer un fichier contenant toutes les informations sur les consommations alimentaires
V1 : Adapatation du fichier de MP (sur les fruits et légumes)
V2 : Suppression des codages de MP sur les fruits et légumes
V2 : amélioré codage légumes et fruits
	ajouté codage lieu repas midi 89
V3 :	ajouté codage lieux repas ap 89 
*/
local tag "cree_alimentation01_v3.do cd 20/05/2015"
********************************************************************************

//	#1
//	garder var pertinentes fichier aq_alim

use "$source\1_DATA_AQ_ALIM_fev2015", clear
save "$temp\t_aq_alim.dta", replace

/*
keep proj_gest_ntt aqannee aq_alim_nba* aq_alim_reg* aq_alim_dejdom aq_alim_lmidi1 aq_alim_typmg1 aq_alim_typeh1  aq_alim_light* aq_alim_nbcafe aq_alim_nbpdj aq_alim_nbrm aq_alim_nbrs aq_alim_mgent1 aq_alim_mgent2 aq_alim_mgent3
*/
* J'enlève les variables alimentaires du FFQ de 1990

drop aq_alim_nba1 aq_alim_nba2 aq_alim_nba3 aq_alim_nba4 aq_alim_nba5 aq_alim_nba6 aq_alim_nba7 aq_alim_nba8
//	#2
//	recodages

label def Lffq 1 "Jamais ou presque" 2 "1 à 2 /semaine" ///
	3 "+ que 2/semaine et < tlj" ///
	4 "Tous les jours ou presque", modify

*indicatrice 1° FFQ
gen ffq1= aqannee==1990
label var ffq1 "FFQ de 1990"
label value ffq1 Lon01
note ffq1: permet de contrôler l'effet des changements de codage après 1990 / `tag'

*identifiant FFQ
recode aqannee (1990=1) (1998=2) (2004=3) (2009=4) (2014=5) (else=.) , gen(ffqn)
label var ffqn "Fréquentiel alimentaire N°"
note ffqn: Pour construire la matrice de covariance il faut le numéro de la visite  ///
	et non l'année. ffqn est MANQUANT s'il n'y a pas de ligne pour l'individu, ///
	l'année du ffq, dans la base AQ_ALIM. Je suppose que l'individu n'a répondu ///
	à aucune question sur l'alimentation cette année-là. / `tag'

/*var sans missing pour faire des sommes
gen i1=aq_alim_nba6
gen i2= aq_alim_nba8
gen i3= aq_alim_nba09
gen i4= aq_alim_nba10
gen i5= aq_alim_nba16

mvencode i1-i5, mv(0)

*légumes
gen lgm14=i1 + i3
recode lgm14(0=.)
label var lgm14 "Nbre de fois/semaine: légumes cuits(verts en 1990)"

label value lgm14 Lffq
note lgm14 : ajoute info de 1990 sur les légumes verts / `tag'

*jamais de légumes

recode lgm14(1=1) (nonmiss=0), gen(lgm14jms)
label val lgm14jms Lon01
label var lgm14jms "Jamais de légumes cuits (verts en 1990)"
note lgm14jms : en 1990, jms de légumes verts. ensuite, jamais de cuit / `tag'


recode lgm14(4=1) (nonmiss=0), gen(lgm14tlj)
label val lgm14tlj Lon01
label var lgm14tlj "Légumes ts les jours"
note lgm14tlj : reponse ts les jours. en 1990, légumes verts. ensuite, cuit / `tag'

*fruits
gen fruit14=i2 + i5
recode fruit14(0=.)
label var fruit14 "Nbre de fois/semaine: fruits frais (yc. 1990)"
label value fruit14 Lffq
note fruit14: ajouté info de 1990 (fruits sans précision) / `tag'

recode fruit14 (1=1) (nonmiss=0), gen(fruit14jms)
recode fruit14 (4=1) (nonmiss=0), gen(fruit14tlj)
label var fruit14jms "Jamais de fruits"
label var fruit14tlj "Fruits ts les jours"
label value fruit14jms fruit14tlj Lon01
note fruit14jms: en 1990 jamais de fruits puis jamais de fruits frais / `tag'
note fruit14tlj: en 1990 ts ls jours ou presque fruits (1990) puis ///
	fruits frais / `tag'

	
	
//	#3	
//	variables originales sous un nom potable
gen lgm90=aq_alim_nba6
gen lgm98=aq_alim_nba09
gen crud98=aq_alim_nba10
gen fruit90 = aq_alim_nba8
gen fruit98= aq_alim_nba16

label var lgm90 "conso légumes verts (1990)"
label var lgm98 "FFQ conso légumes cuits (1998sq)"
label var crud98 "FFQ conso légumes cuits (1998sq)"
label var fruit90 "FFQ conso fruits (1990)"
label var fruit98 "FFQ conso fruits frais(1998)"

label value lgm90-fruit98 Lffq

note lgm90 : Copie deaq_alim_nba6 / `tag'
note lgm98 : Copie deaq_alim_nba09 / `tag'
note crud98 : Copie deaq_alim_nba10 / `tag'
note fruit90  : Copie de aq_alim_nba8 / `tag'
note fruit98 : Copie de aq_alim_nba16 / `tag'

//	#4
//	variables "quanti"

foreach var of varlist lgm14 fruit14 crud98 lgm98 fruit98 {
	gen `var'q = `var'
	recode `var'q (1=0) (2=2) (3=4) (4=7)
	note `var'q: `var' recodé en nombre effectif de fois par semaine. jamais=0, ///
	1à 2 fois=2, >2fois=4, tlj=7 / `tag'
}

label var lgm14q "Légumes (cuits) par semaine code continu"
label var fruit14q "Fruits par semaine code continu"
label var crud98q "Crudités par semaine code continu (98sq)"
label var lgm98q "Légumes cuits par semaine code continu(98sq)"
label var fruit98q "Fruits frais par semaine code continu (98sq)"
*/




//	#5	
//	régime prescrit
gen yregime=.
replace yregime=1 if aq_alim_regime==1
replace yregime=0 if aq_alim_regime==2
replace yregime=1 if aq_alim_regdia==1 | aq_alim_regcho==1 | aq_alim_reghyp==1 | aq_alim_reghur==1 ///
	| aq_alim_regami==1 | aq_alim_regaut==1 
replace yregime=0 if aq_alim_regnon==1
label var yregime "Régime prescrit"
label val yregime Lon01
note yregime: individu suit un régime prescrit par un médecin / `tag'


//	#6
//	lieu repas midi baseline
gen dejdom89= aq_alim_dejdom
recode dejdom89 (2=0)
label var dejdom89 "Déjeuner généralt au domicile(89)"
label val dejdom89 Lon01
note dejdom89 : / `tag'

//	#7
//	lieu autres repas
gen ydejdom=aq_alim_lmidi1==3
replace ydejdom=. if aq_alim_lmidi1==.
carryforward dejdom89, gen(dd)
replace ydejdom=dd if aqannee==1990

label var ydejdom "Déjeune le + svt au domicile"
label val ydejdom Lon01
note ydejdom: 1990 : copié la valeur de dejdom89 (codé depuis aq_alim_dejdom), question posée en 1989. ///
	1998 et suivantes:  codé à partir de aq_alim_lmidi1. il y a pls variables pour les lieux de repas. ///
	apparemment ctnes pers ont coché plusieurs cases alors que la var n'est pas prévue pour être ordonnée ///
	du coup j'imagine que si pls cases cochées, on ne peut pas savoir quel est le lieu LE PLUS fréquent. ///
	c le cas pour 747 lignes, j'estime que c'est négligeable. `tag'

/*drop i1-i5 dd
drop aq_alim_nba6 aq_alim_nba8 aq_alim_nba09 aq_alim_nba10 aq_alim_nba16 aq_alim_reg* aq_alim_dejdom aq_alim_lmidi1*/
/*order  proj_gest_ntt aqannee ffq1 ffqn lgm1* lgm9* crud* fruit1* fruit9* dejdom89 ydejdom*/
compress
sort proj_gest_ntt aqannee
save "$cree\alimentation01", replace


