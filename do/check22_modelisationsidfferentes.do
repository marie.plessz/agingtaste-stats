/*
********************************************************************************
*	program:	check22_modelisationsdifferentes
*	project:	AGT
*	author:		MP
*	date: 		2021-07-05
*   TASK : keep a copy of all the other model specifications tested

********************************************************************************
*/

*================	Household size		====================*

* au minimum contrôler par ynper (continu)
*conjoint et enfants





* ne pas utiliser nenftot il n'est pas vraiment dépendant du temps (source GPSO sans doute pas mis à jour après retraite)

forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yageC c.yageC#i.diplom3 i.diplom3 i.generation i.ycouple i.yautpresents  ///
			if femme==`s' & inclu==1 &	yage>49  ///
			|| proj_gest_ntt : 
	
		eststo X`v'F`s'
		estat icc
		quietly margins i.diplom3, at((base) _factor yageC = (-10(5)10) )
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot, xlabel(-10 "50" -5 "55" 0 "60" 5 "65" 10 "70" 15 "75 ans") ///
			xtitle(Age) title("Axe `v' - Femme = `s'") ///
			saving("$res/pourCetS_explo_X`v'F`s'", replace) ///
			nodraw
	}
}


set dp comma
esttab X*F*, not label mtitles(H1 H2 H3 F1 F2 F3) b(a2)


grc1leg 		///
	"$res/pourCetS_explo_X1F0"	///
	"$res/pourCetS_explo_X1F1"	///
	"$res/pourCetS_explo_X2F0"	///
	"$res/pourCetS_explo_X2F1"	///
	"$res/pourCetS_explo_X3F0"	///
	"$res/pourCetS_explo_X3F1"	///
	, span scheme(s1mono) cols(2)  scale(0.5) iscale(1) xcommon imargin(b=0 t=0)


*==========		rupture de pente à 60 ans	===============*	
*modèle avec rupture de pente à 60 ans, et différences de pente selon diplôme seult après 60 ans
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yage1B c.yage2B#i.diplom3 i.diplom3  i.generation if femme==`s' & inclu==1  || proj_gest_ntt : 
		eststo X`v'F`s'
		estat icc
		quietly margins i.diplom3, at((base) _factor yage2B=(0(5)15) yage1B = 0 ) 
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot, xlabel(0 "60" 5 "65" 10 "70" 15 "75 ans") ///
			xtitle(Age) title("Axe `v' - Femme = `s'")  scheme(s1mono)  ///
			saving("$res/pourCetS_explo_X`v'F`s'", replace) 
	}
}
	
* Modèle spécifié avec tendance commune et interactions en plus après 60 ans (permet mieux de voir les écarts significatifs)
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yage1B c.yage2B##i.diplom3  i.generation if femme==`s' & inclu==1  || proj_gest_ntt : 
		eststo X`v'F`s'
		estat icc

	}
}
set dp comma
esttab X*F*, not label mtitles(H1 H2 H3 F1 F2 F3) b(a2)

*==============		other specs for education/class 	===================*

**** modèle sur l'origine sociale : moins intéressant car effets moins significatifs
/* commentaire général : l'orig sociale (gpe pro du père) a moins d'effet que le diplôme.
j'ai essayé avec l'orig sociale en 3 modalités + "inconnu", en 2 modalité (moyen et haut vs bas et inconnu)
et en 2 mobalité (haut vs tout le reste). résultats moins faciles à interpréter que diplôme.
*/
recode statutpere (1 2 7 . =0 "pere bas") (3 = 1 "pere haut"), gen(perehaut)

forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yage1B c.yage2B#i.perehaut i.perehaut  i.generation if femme==`s' & inclu==1  || proj_gest_ntt : 
		eststo X`v'F`s'
		estat icc
		quietly margins i.perehaut, at((base) _factor yage2B=(0(5)15) yage1B = 0 ) 
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot, xlabel(0 "60" 5 "65" 10 "70" 15 "75 ans") ///
			xtitle(Age) title("Axe `v' - Femme = `s'") ///
			saving("$res/pourCetS_explo_X`v'F`s'", replace) 
	}
}

**** modèle avec diplôme en 2 modalités : 

/*l'écart entre BEPC et BEP est faible dans les modèles/ les écarts d'origine sociale  
sont  faibles aussi.
je regroupe BEPC et BEP pour voir : on peut raconter la même histoire.
*/

* le modèle à 2 modalités en dit autant que celui à 3 : 
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yage1B c.yage2B#i.bacouplus i.bacouplus  i.generation if femme==`s' & inclu==1  || proj_gest_ntt : 
		eststo X`v'F`s'
		estat icc
		quietly margins i.bacouplus, at((base) _factor yage2B=(0(5)15) yage1B = 0 ) 
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot, xlabel(0 "60" 5 "65" 10 "70" 15 "75 ans") ///
			xtitle(Age) title("Axe `v' - Femme = `s'") ///
			saving("$res/pourCetS_explo_X`v'F`s'", replace) 
	}
}

