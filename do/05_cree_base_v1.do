********************************************************************************
*	program:  5_cree_base_v1
*	project: AGT
*	author: MP
* Adaptation du fichier de MP par CD le 20/05/2015 pour intégration des données 2014. 
* repris par MP le 06/07/2021
/*	task:
ajouter vin 
imputer ynenf, ycouple, 
sélectionner années FFQ
creer aliments
imputer aliments
*/
local tag "5_cree_base_v1"
********************************************************************************

* Fusion avec aq_alcool_complet pour récupérer les variables en détail sur l'alcool

use "$source\1_DATA_AQ_fev2015.dta", clear
save "$source\alcool_complet2014.dta", replace
keep proj_gest_ntt suivi_aq_aqannee aq_gener_bv aq_gener_bb aq_gener_ba aq_gener_vv aq_gener_vnj aq_gener_vb aq_gener_bnj aq_gener_va aq_gener_anj aq_gener_alcool2
rename suivi_aq_aqannee aqannee
sort  proj_gest_ntt aqannee 
save,replace

use "$cree\ALV02.dta", clear
sort  proj_gest_ntt aqannee
merge 1:1 proj_gest_ntt aqannee using "$source\alcool_complet2014"
drop _merge


*************************0 - compléter ynenf et ycouple ************


bys i (a) : carryforward(ynenf), replace
label var ynenf "Nb d'enfants au foyer (max 5, complété)"
note ynenf : complété par carryforward

bys i (a): carryforward(ycouple), replace
label var ycouple "En couple (complété)"
note ycouple : complété par carryforward


*********** garder années pertinentes *************************


keep if aqannee ==1998 | aqannee == 2004 | aqannee == 2009 | aqannee == 2014

save "$cree\01_cree_base.dta", replace

////////////////////////////////////////////////////////////////////////////////

*************************.1. Creation des variables******************************* 
use "$cree\01_cree_base.dta", clear



* Variables retraite : après 1er Janv. 1990 
recode retannee (. = 9999 )
generate ret90=retannee if a==1989 

bys i : carryforward ret90, replace
recode ret90 (1979/1989=0)(1990/2015=1) 
label variable ret90 "Retraite après 1990"
label define ret90 0"Avant 1990" 1"Après 1990"
label value ret90 ret90


generate ret89= retannee if a==1989
bys i : carryforward ret89, replace
recode ret89 (1979/1988=0)(1989/2015=1)
label define ret89 0"Avant 1989" 1"Après 1989"
label value ret89 ret89

* Variable dessert en fusionnant "dessert sucré" et "dessert biscuit" cf Agnes Le Port

gen aq_alim_nba15=1 if (aq_alim_nba151==1 & aq_alim_nba152==1)|(aq_alim_nba151==1 & aq_alim_nba152==.)|(aq_alim_nba151==. & aq_alim_nba152==1)
replace aq_alim_nba15=2 if (aq_alim_nba151==2 & aq_alim_nba152==1)|(aq_alim_nba151==1 & aq_alim_nba152==2)|(aq_alim_nba151==. & aq_alim_nba152==2)|(aq_alim_nba151==2 & aq_alim_nba152==.)
replace aq_alim_nba15=3 if (aq_alim_nba151==3 & aq_alim_nba152==1)|(aq_alim_nba151==2 & aq_alim_nba152==2)|(aq_alim_nba151==3 & aq_alim_nba152==2)|(aq_alim_nba151==1 & aq_alim_nba152==3)| ///
(aq_alim_nba151==2 & aq_alim_nba152==3)|(aq_alim_nba151==. & aq_alim_nba152==3)|(aq_alim_nba151==3 & aq_alim_nba152==.)
replace aq_alim_nba15=4 if (aq_alim_nba151==4 & aq_alim_nba152==1)|(aq_alim_nba151==4 & aq_alim_nba152==2)|(aq_alim_nba151==3 & aq_alim_nba152==3)|(aq_alim_nba151==4 & aq_alim_nba152==3)| ///
(aq_alim_nba151==1 & aq_alim_nba152==4)|(aq_alim_nba151==2 & aq_alim_nba152==4)|(aq_alim_nba151==3 & aq_alim_nba152==4)|(aq_alim_nba151==4 & aq_alim_nba152==4)|(aq_alim_nba151==. & aq_alim_nba152==4)|(aq_alim_nba151==4 & aq_alim_nba152==.)
replace aq_alim_nba15=. if (aq_alim_nba151==. & aq_alim_nba152==.)

*Création de la variable light recodée en 1 variable à 3 modalités
recode aq_alim_light1( 2=0), gen(light1)
recode aq_alim_light2( 2=0), gen(light2)
recode aq_alim_light3( 2=0), gen(light3)
recode aq_alim_light4( 2=0), gen(light4)
recode aq_alim_light5( 2=0), gen(light5)
recode aq_alim_light6( 2=0), gen(light6)
recode aq_alim_light7( 2=0), gen(light7)
recode aq_alim_light8( 2=0), gen(light8)

egen lightb = rowtotal(light1  light2  light3  light4  light5 light6 light7 light8) if light1 !=. & light2 !=. & light3 !=. & light4 !=. & light5 !=. & light6 !=. & light7 !=. & light8 !=. 
drop aq_alim_light1 aq_alim_light2 aq_alim_light3 aq_alim_light4 aq_alim_light5 aq_alim_light6 aq_alim_light7 aq_alim_light8  light1 light2 light3 light4 light5 light6 light7 light8
recode lightb ( 2=1) (3 4 5 6 7 8=2), generate (aq_alim_nba23)
label variable aq_alim_nba23 "Nb produits light recodé"
label define light 0"Aucun" 1"1 à 2"2"3 et plus"
label value aq_alim_nba23 light

* Création variable cafe recodée en 3 modalités : aucun, 1 à 2, 3 et +

recode aq_alim_nbcafe ( 2=1) ( 3/30 = 2), generate (aq_alim_nba22)
label value aq_alim_nba22 light


* Creation variable vin : asbtinent, boit mais pas ts les jrs, boit ts les jrs
gen aq_alim_nba24=1 if aq_gener_bv==2
replace aq_alim_nba24=2 if aq_gener_bv==1 & aq_gener_vnj<7 
replace aq_alim_nba24=3 if aq_gener_bv==1 & aq_gener_vnj==7 

*Création de la variable déjeuner à domicile

generate byte dejdom=(aq_alim_lmidi1==3) if !missing(aq_alim_lmidi1)
label variable dejdom "Déjeune à domicile"
label define ON 0"Non" 1"Oui", replace
label value dejdom ON

////////////////////////////////////////////////////////////////////////////////

/*******************.2.Imputation sur le mode************************************

On tiens compte de : sexe, âge,pcs,situation conjugale, diplôme (cf Agnes Le Port) + année aq
 
*/

* Préparation des variables pour l'imputation
* Suppression des variables de 1990
drop aq_alim_dejdom 
drop aq_alim_regnon aq_alim_regdia aq_alim_regcho aq_alim_reghyp aq_alim_reghur aq_alim_regami aq_alim_regaut
*Renommer les variables pour la macro ( ne reconnaît pas les 01, 02 etc..)
rename aq_alim_nba01 aq_alim_nba1
rename aq_alim_nba02 aq_alim_nba2
rename aq_alim_nba03 aq_alim_nba3
rename aq_alim_nba04 aq_alim_nba4
rename aq_alim_nba05 aq_alim_nba5
rename aq_alim_nba06 aq_alim_nba6
rename aq_alim_nba07 aq_alim_nba7
rename aq_alim_nba08 aq_alim_nba8
rename aq_alim_nba09 aq_alim_nba9
rename aq_alim_typmg1 aq_alim_nba21

* Critère de sélection : réponse à plus de 7 questiosn alimentaires, on impute les non réponses seulement pour ces individus
misstable summarize ( aq_alim_nba1 aq_alim_nba2 aq_alim_nba3 aq_alim_nba4 aq_alim_nba5 aq_alim_nba6 aq_alim_nba7 aq_alim_nba8 aq_alim_nba9 aq_alim_nba10 aq_alim_nba11 aq_alim_nba12 aq_alim_nba13 aq_alim_nba14 aq_alim_nba151 aq_alim_nba152 aq_alim_nba16 aq_alim_nba17 aq_alim_nba18 aq_alim_nba19 aq_alim_nba20 aq_alim_nba21 aq_alim_nba22 aq_alim_nba23 aq_alim_nba24 ), generate(miss_)
egen mis3=rowtotal(miss_*)
tab mis3 aqannee
label variable mis3 "Nombre de manquant au Var Alim"
generate complet=(mis3==0)
label value complet ON
label variable complet "Nb ind qui ont répondu à tout"
generate missimp = (mis3<=7)
label value missimp ON
label variable missimp "Ont répondu à + de 7 question alimentaire"

*Critère d'imputation : tranche d'âge, sexe, pcs à l'entrée, situtation conjugale, bac ou pas, année FFQ)
 * Tranche d'âge en 3 catégorie : (en 1998) 45-50 ans / 50-55 ans / 55-60ans
/*gen age98=1998-annais
gen agecl=1 if age98>=45 & age98<50
replace agecl=2 if age98>=50 & age98<55
replace agecl=3 if age98>=55 & age98<60

* macro d'imputation

egen group_impu = group(agecl femme pcs_entree ycouple bacouplus aqannee )
*/

recode yage6 (1=2), gen(yage5)

egen group_impu = group(yage5 femme ycouple  diplom3  )

forvalues i = 1/24{
	by group_impu, sort: egen mod`i' = mode(aq_alim_nba`i'), maxmode 
	replace aq_alim_nba`i' = mod`i' if aq_alim_nba`i' ==. & missimp ==1
	}
	

drop yage5
drop miss_*
drop mod*
drop group_impu

////////////////////////////////////////////////////////////////////////////////
*******************.3. Labels et variables DU FFQ alimentaire*******************

*Renommer les variables du ffq

rename aq_alim_nbpdj nbpdj
rename aq_alim_nbrm nbrm
rename aq_alim_nbrs nbrs
rename aq_alim_mgent1 collam
rename aq_alim_mgent2 collpm
rename aq_alim_mgent3 collngt
rename aq_alim_nba1 viande
rename aq_alim_nba2 volaille
rename aq_alim_nba3 charcuterie
rename aq_alim_nba4 poisson
rename aq_alim_nba5 oeuf
rename aq_alim_nba6 friture
rename aq_alim_nba7 beurre
rename aq_alim_nba8 feculent
rename aq_alim_nba9 legumect
rename aq_alim_nba10 crudite
rename aq_alim_nba11 huile
rename aq_alim_nba12 lait
rename aq_alim_nba13 laitage
rename aq_alim_nba14 fromage
rename aq_alim_nba15 dessert
rename aq_alim_nba151 dessertscre
rename aq_alim_nba152 dessertbsct
rename aq_alim_nba16 fruit
rename aq_alim_nba17 pain
rename aq_alim_nba18 viennoiserie
rename aq_alim_nba19 sucre
rename aq_alim_nba20 boissonsucree
rename aq_alim_nba21 matieregrasse
rename aq_alim_nba22 cafe
rename aq_alim_nba23 light
rename aq_alim_nba24 vin
rename aq_alim_lmidi1 lieudejeuner
rename aq_alim_regime regime
rename aq_gener_alcool2 yalcool
rename suivi_cpdom ycpdom
rename suivi_cptrav ycptrav
*rename dismarch ydismarch
*rename nbrvufam ynbrvufam
format datnais %td

* Simplification des labels alimentaires

label variable viande "Nb Viande/ semaine"
label variable volaille "Nb Volaille/ semaine"
label variable charcuterie "Nb Charcuterie / semaine"
label variable poisson "Nb Poisson/semaine"
label variable oeuf "Nb Oeuf/semaine"
label variable friture "Nb Fritures/ semaine"
label variable beurre "Nb Beurre/semaine"
label variable feculent "Nb Feculent/semaine"
label variable legumect "Nb Legumes Cuits/semaine"
label variable crudite "Nb Crudités/semaine"
label variable huile "Nb Huile/semaine"
label variable lait "Nb Lait/semaine"
label variable laitage "Nb Laitages/semaine"
label variable fromage "Nb Fromage/semaine"
label variable dessertscre "Nb Dessert sucrés/semaine"
label variable dessertbsct "Nb Dessert biscuit patisseries/semaine"
label variable fruit "Nb Fruits frais/semaine"
label variable pain "Nb Pain/ semaine"
label variable viennoiserie "Qt Viennoiseries/semaine"
label variable sucre "Qt Sucre/jours"
label variable boissonsucree "Qt Boissons sucrées/jours"
label variable matieregrasse "Matière grasse utilisée le + souvent"
label variable cafe "Nb café/jours"
label variable yalcool "Classe de buveur"
label variable vin "Quantité vin/jrs"
label variable ycpdom "Code postal domicile chaque année"
label variable ycptrav "Code postal travail chaque année"
*label variable ydismarch "Distance parcourue en marchant"
*label variable ynbrvufam "Nombre de fois vue des proches/familles"

* Label des modalités 

label define Lffq 1 " Jamais ou presque" 2 " 1 à 2 fois/sem" 3 " + de 2 fois/sem" 4 "Ts les jrs ou presque", modify
label value viande volaille charcuterie poisson oeuf friture beurre feculent legume crudite huile lait laitage fromage dessertscre dessertbsct fruit pain Lffq 

label define viennoiserie 1 " Aucun" 2 " 1 à 3" 3 "4 à 6" 4 "7 ou+"
label value viennoiserie viennoiserie

label define sucre 1"Aucun" 2" 1 ou 2" 3 " 3 ou 4" 4" 5 ou plus"
label value sucre sucre

label define boisson 1"Aucune" 2"Moins de 1/2 Litres" 3 "1/2 à 1L" 4" 1L ou +"
label value boissonsucree boisson

label define nbrepas 1 "Jamais " 2 "1 à 3 fois/semaine" 3" 4 à 6 fois/semaine" 4 "Tous les jours de la semaine"
label value nbpdj nbrm nbrs  nbprepas

label define mgent 1"Jamais" 2"Rarement" 3"Souvent" 4"Toujours"
label value collam collpm collngt mgent

label define typmg 1"Beurre" 2"Huile" 3"Margarine" 4" Autres"
label value matieregrasse typmg


////////////////////////////////////////////////////////////////////////////////
***********************.4.Nettoyage de la base**********************************


* Nettoyage des données Diapason autres que cas et témoins principaux et secondaires
/*drop suivi_pop_sexe vivant2012 annee_prel annee_ret imc nb_criteres_ncep syndrome_meta_ncep nbaq2008 trigly_decla diab_decla nb age_c imc_c reg hypertension_decla
*/
*Nettoyage variables alimentaires
/*drop aq_alim_lmidi2 aq_alim_lmidi3 aq_alim_lmidi4 aq_alim_lmidi5 aq_alim_lmidi6 
drop aq_alim_typmg2 aq_alim_typmg3
drop aq_alim_typeh1 aq_alim_typeh2 aq_alim_typeh3 aq_alim_typeh4 aq_alim_typeh5 aq_alim_typeh6
*/
drop aq_alim_nbcafe dessertscre dessertbsct lightb lieudejeuner
drop aq_alim_typmg2 aq_alim_typmg3 aq_alim_typeh1 aq_alim_typeh2 aq_alim_typeh3 aq_alim_typeh4 aq_alim_typeh5 aq_alim_typeh6 aq_alim_lmidi2 aq_alim_lmidi3 aq_alim_lmidi4 aq_alim_lmidi5 aq_alim_lmidi6
* Nettoyage fichier RL02 reorganisation de RLO2 (variables socio-démo)
/*drop  dejdom89 retlieudej ydejdom lgm14 lgm14tlj lgm14jms fruit14 fruit14tlj fruit14jms lgm14q lgm90 lgm98 lgm98q fruit14q fruit98 fruit98q crud98 crud98q fruit90*/

/*drop imcclbl ysurpoids poidscorr taille90 yimc imc9092 yetats yphyfat ynerfat ytsatis ytnerfa ytphyfa ytnerfacat ytphyfacat etatsblcat yetatscat etatscat89 etatsbl mvsesantebl ynerfatcat formret fumeur89 alcool89 yphyfatcat

drop /*yr_recup recup recup_jours retdate_recup retannee_recup*/ 
*/
/*drop merge1 merge2 merge3 merge4 merge5 merge6 */
* drop suivi_aq_aqenv suivi_aq_aqrep 
drop yetats yphyfat ynerfat ytsatis ytnerfa ytphyfa

* Réogranisation du fichier

drop aq_gener_bv aq_gener_bb aq_gener_ba aq_gener_vv aq_gener_vnj aq_gener_vb aq_gener_bnj aq_gener_va aq_gener_anj yalcool

*order proj_gest_ntt i  aqannee a aqdate ffqn aqrep sortieraison sortiedate invaliditedate invdate invlmmp ffq1  flag_ligne complet missimp mis3 femme generation agecl yage6 yage age98 annais datnais neap45 ynper yactif  yactif2 retraite ret90 ret89 retannee vague jagecatdet yr retdate ffqnj jage jagecat diplom bacouplus diplom3 nbhab_92 revenu_89 revenu_02 revpc_89 revpc_02 gf1_entree jgf1 gf1_mob gf1_35 pcs_entree pcs_35 jpcs proouv statutpere proper ycouple couplebl statutcjtbl yconj_actif /*epouseb*/ conj_pro_89 ev_retconjoint  ynenf dejdom89 retlieudej ydejdom imcbl3 jimc3 jetatscat yregime regime dejdom nbpdj nbrm nbrs collam collpm collngt viande volaille charcuterie poisson oeuf friture feculent legumect crudite lait laitage fromage dessert fruit pain viennoiserie sucre boissonsucree cafe vin light beurre huile matieregrasse


********************************************************************************

save "$cree\01_cree_base.dta", replace

********************************************************************************
