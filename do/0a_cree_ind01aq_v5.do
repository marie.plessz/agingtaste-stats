********************************************************************************
// program:  cree_ind01aq_v5.do
// project: ALV
// author: CD
/* task: creer le fichier t_ind01aq.dta
ce fichier doit contenir les variables extraites de l'autoquestionnaire 
qui ne varient pas dans le temps
il ne contient qu'une seule ligne par individu

le 30/10: je modifie conj_pro_89 pour que les manquantes soient manquantes
le 18/11 ajouté codage alcool
le 11/12: ajouté statutpere et statutcjtbl
v4: fumeur89 complété ac Aq de 90 et 91. id pour l'alcool

*/
local tag "cree_ind01aq_v5.do cd 20/05/2015"
*******************************************************************************
set more off
//	#0 
// 	donner des noms commodes aux ind et aux dates
use "$source\1_DATA_AQ_fev2015.dta", clear
save "$temp\t_01aq.dta", replace // modifié pour résoudre pb dans trajectoire le 2019-04-08
gen long i=proj_gest_ntt
gen a=suivi_aq_aqannee


// #1
// Garder les variables qui viennent du fichier DATA_AQ et les années 
//jusqu'au dernier FFQ


keep i a proj_gest_ntt ///
	aq_gener_datr ///
	suivi_aq_aqannee ///
	suivi_aq_aqenv ///
	suivi_aq_aqrep ///
	aq_entfam_proper ///
	aq_gener_cprof	 ///
	aq_gener_procon	 ///
	aq_gener_sitcon	 ///
	aq_sitsoc_diplom	 ///
	aq_prof_proouv	 ///
	aq_gener_sitfam	 ///
	aq_gener_nper	 ///
	aq_envvie_nbhab	 ///
	aq_sitsoc_revenu	 ///
	aq_sitsoc_netrev	 ///
	aq_sitsoc_sitsoc 	///
	aq_sitsoc_sitso2	///
	aq_gener_fum aq_gener_alcool1

keep if a<2015

// #2
// recodages pour ramener à la ligne 1989 les infos ponctuelles

//profession conjoint en 1989 : conj_pro_89

gen conj_pro_89=	aq_gener_procon 
replace conj_pro_89=0 if ~inlist(aq_gener_sitfam, 2,3) & aq_gener_procon ==.
replace conj_pro_89=8 if aq_gener_cprof==2
recode conj_pro_89 (2=1) (7 =.)(8=7)

label var conj_pro_89 "Situation conjoint en 1989"
label def conj_pro_89 ///
	0 "Pas de conjoint" ///
	1 "Agriculteur, indépendant" ///
	3 "Cadre" ///
	4 "Prof. intermédiaire" ///
	5 "Employé" ///
	6 "Ouvrier" ///
	7 "Hors emploi" , modify
label value conj_pro_89 conj_pro_89
note conj_pro_89: inclut l'info sur vie en couple. corrigé pour que ///
	les valeurs manquantes soient effectivement manquantes. réponse "autre" ///
	aussi codée manquante car trop rare	/ `tag'


	
local vardrop "i a aq_gener_cprof aq_gener_procon aq_gener_sitcon"

//taille agglo 1992= nbhab_92
bys proj_gest_ntt : egen nbhab_92=sum(aq_envvie_nbhab)
recode nbhab_92 (0=.)
label var nbhab_92 "Taille agglo en 1992"
label def Lnbhab 1 "1_Rural" 2 "2_2k-5k hab" 3 "3_5k-30k hab" ///
	4 "4_30k-100k hab" 5 "5_>100k hab", modify
label value   nbhab_92 Lnbhab
note nbhab_92: `tag'

local vardrop "`vardrop' a aq_envvie_nbhab"

//taille ménage NPER
gen nper=aq_gener_nper
recode nper (0 = 1) ( 99=.)(8/98=7)
label var nper "Taille ménage AQ (max 7)"
note nper: `tag'

//revenus en 1989 et en 2002
bys i : egen revenu_89=sum(aq_sitsoc_revenu)
bys i : egen revenu_02=sum(aq_sitsoc_netrev)

recode revenu_02 (1 = 	743	) ///
	(2 = 	1067	) ///
	(3 = 	1258	) ///
	(4 = 	1486	) ///
	(5 = 	1791	) ///
	(6 = 	2287	) ///
	(7 = 	3201	) ///
	(8 = 	4192	) ///
	(9 = 	5336	) ///
	(10 = 	9147	) ///
	(0=.)

recode revenu_89	(1 = 	571	) ///
	(2 = 	876	) ///
	(3 = 	1067	) ///
	(4 = 	1257	) ///
	(5 = 	1486	) ///
	(6 = 	1791	) ///
	(7 = 	2286	) ///
	(8 = 	3201	) ///
	(9 = 	5717	) ///
	(0=.)

label var revenu_89 "Revenu mensuel AQ1989"
label var revenu_02 "Revenu mensuel net AQ2002"
note revenu_89: revenu du ménage en euros courants. milieu de la tranche sauf ///
tranche 1 (max*0.75) et dernière (min*1.5). `tag'
note revenu_02: revenu du ménage en euros courants. milieu de la tranche sauf ///
tranche 1 (max*0.75) et dernière (min*1.5). `tag'

//revenu par tête 1989
gen revpc_89= revenu_89/sqrt(nper)
replace revpc_89=. if a!=1989
replace revpc_89=3000 if revpc_89>3000 & revpc_89<.
label var revpc_89 "Revenu mensuel par tête 1989"
note revpc_89: revenu_89/sqrt(nper) topcodé à 3000. `tag'

//revenu par tête 2002
carryforward nper, gen(d)
gen c= revenu_02/sqrt(d)
replace c=4000 if c>4000 & c<.
*replace c=. if a!=2002
gen y=(a==2002)*c
bys i: egen revpc_02=sum(y)
replace revpc_02=. if c==.
label var revpc_02 "Revenu mensuel par tête 2002"
note revpc_02: revenu_02/sqrt(nper) topcodé à 4000. si le nb de pers ///
	dans le foyer était manquant, on l'a "carryforward".`tag'

local vardrop "`vardrop' c y nper d"



label def Lon12 1"Oui" 2 "Non", modify
gen proouv=aq_prof_proouv==1
label value proouv  Lon01 
note proouv: copie en dummy de aq_prof_proouv / `tag'
label var proouv "A réussi cours Promotion Ouvriere"

gen proper= aq_entfam_proper
recode proper (3=2) (4=3)(5=4) (6=5) (7=6) (8=7)
label def proper ///
	1	"Agriculteur expl."	///
	2	"Indépendant/chef entr."	///
	3	"Cadre"	///
	4	"Prof. intermédiaire"	///
	5	"Employé"	///
	6	"Ouvrier"	///
	7	"Autre/inconnu"	, modify
label value proper proper
note proper: copie de aq_entfam_proper, chef entreprises>10 salariés ac les indépendants / `tag'
label var proper "Profession du père"

*DIPLOME
gen diplom= aq_sitsoc_diplom
label var diplom "Diplôme détaillé"
note diplom: copie de aq_sitsoc_diplom `tag'

label def diplom ///
	1	"certificat d’étude primaire"	///
	2	"BEPC"	///
	3	"baccalauréat"	///
	4	"CAP"	///
	5	"BEP, BP, BEC, BEI"	///
	6	"Supérieur technique (BTS etc)"	///
	7	"Autre enseignement supérieur"	///
	8	"Autre diplôme"		, modify
label value diplom diplom

*FUMEUR en 89

bys i (a) : egen f89=sum(aq_gener_fum*(a==1989))
bys i (a) : egen f90=sum(aq_gener_fum*(a==1990))
bys i (a) : egen f91=sum(aq_gener_fum*(a==1991))
recode f89 f90 f91 (0=.)
gen fumeur89 =f89
replace fumeur89=f90 if f89==.  & f90!=.
replace fumeur89=f91 if f89==. & f90==. & f91!=.
recode fumeur89 (1 3=1)(2=0)
label value fumeur89 Lon01
label var fumeur89 "A été fumeur (AQ 89-91)"
note fumeur89: se déclare fumeur ou ex-fumeur en 89. si en 89 NR , je regarde 1992, puis 1991, et je remplace ///
	les 'ex-fumeurs' sont codés comme fumeurs. `tag'
drop f89 f90 f91

/*
replace fumeur89=1 if (aq_gener_fum==1 |aq_gener_fum==3) & suivi_aq_aqannee==1989
replace fumeur89=0 if aq_gener_fum==2 & suivi_aq_aqannee==1989
replace fumeur89=
label value fumeur89 Lon01
label var fumeur89 "Fumeur en 1989"
note fumeur89:`tag'
*/

*ALCOOL en 1989
clonevar alcool89=aq_gener_alcool1
replace alcool89=. if suivi_aq_aqannee!=1989
label def alcool89 0 "Abstinent" 1 "Occasionnel" 2 "Petit buveur" ///
	3 "Moyen buveur" 4 "Gros buveur" 5 "Autre"
label val alcool89 alcool89
label var alcool89 "Classe Buveur 1989"
note alcool89: clone de q_gener_alcool1. `tag'
note alcool89: copie de aq_gener_alcool1. 1=’Occasionnel’ 	Au – une conso occasionnelle mais aucune au quotidien ///
	2=’Petit buveur’  	1 à 2 (Homme) ou 1 (Femme) verres vin ou bière/jour ///
	3=’Moyen buveur’  3 à 4 (Homme) ou 2-3 (Femme) verres vin ou bière/jour ///
	4=’Gros buveur’   	>=5 (Homme) ou >=4 (Femme) verres vin ou bière/jour ///
			Ou	>= 1 pastis /jour Ou >= 1 whisky /jour Ou >= 1 apéro /jour. `tag'

*  du conjoint PROPRE en 3 classes en 89	
gen statutcjtbl=	aq_gener_procon
recode statutcjtbl (5 6=1) (1 4 7=2)(2 3=3)
replace statutcjtbl=7 if aq_gener_cprof==2
replace statutcjtbl=0 if ~inlist(aq_gener_sitfam, 2,3)
label var statutcjtbl "Statut professionnel du conjoint en 1989"
label def statutcjtbl 0 "0_Pas de conjoint" 1 "1_Bas" 2 "2_Moyen" 3 "3_Haut" 7 "7_Hors emploi"
label val statutcjtbl statutcjtbl
note statutcjtbl: Recode de aq_gener_procon. 0 si sitfam pas couple en 89, 1 si procon=employé ou ouvrier ///
	2 si procon= cadre intermédiaire, agriculteur ou "autre", 3 si procon= cadre sup ou indépendant, ///
	7 si cprof=2 cad si déclare que cjt travaille pas actuellement. manquantes sont manquantes (78 cas). `tag'

*	STATUT du père en 3 clases
gen statutpere=proper
recode statutpere (5 6=1) (1 4 =2)(2 3=3) (7=7)
label var statutpere "Statut professionnel du père"
label def statutpere 1 "Bas" 2 "Moyen" 3 "Haut" 7 "7_Autre/inconnu"
label val statutpere statutpere
note statutpere : recode de proper.1 si procon=employé ou ouvrier ///
	2 si procon= cadre intermédiaire, agriculteur , 3 si procon= cadre sup, chef entrep ou indépendant, ///
	7 si proper = autre/inconnu. `tag'

/* 
*inutile d'aller chercher les aq suivants car 1 seule NR en 1989)
bys i (a) : egen a89=sum((aq_gener_alcool1 +1)*(a==1989))
bys i (a) : egen a90=sum((aq_gener_alcool1 +1)*(a==1990))
bys i (a) : egen a91=sum((aq_gener_alcool1 +1)*(a==1991))
recode a89 a90 a91 (0=.)

gen alcool89 =a89 
replace alcool89 =a90  if a89==.  & a90!=.
replace alcool89 =a91 if a89==. & a90==. & a91!=.
replace alcool89=alcool89-1
drop a89 a90 a91
*/


//#3
//garder ligne 1989
keep if a==1989
sort i a
cap drop `vardrop'
compress
save  "$temp\t_ind01aq", replace





//#4
//nettoyage, recodage, labels

drop suivi_aq_aqenv suivi_aq_aqrep aq_gener_datr aq_sitsoc_sitsoc ///
	aq_sitsoc_sitso2 aq_sitsoc_netrev


drop aq_entfam_proper aq_sitsoc_diplom aq_prof_proouv aq_gener_fum aq_gener_alcool1


sort proj_gest_ntt
compress
save  "$temp\t_ind01aq.dta", replace
exit

