* 10_AGT_modeles
local tag "10_AGT_modeles"
cap log close
log using "$res/log_`tag'.txt", text replace
/*
MP le 15/06/2021
à partir de Geronto et société, peut-on capter les divergences/ convergences sln variable intéressante sur cq axe?
*/


		  
* modèles sur données "long"
local tag "10_AGT_modeles"

use "$cree\9_AGT_axes.dta", clear

keep proj_gest_ntt a aqrep femme a98axe* diplom3 yage* generation ycouple yenf01
	   
*==========		Modèles préférés		===============*	

*** modèle avec âge du début à la fin, à partir de 50 ans (avant, pas assez de points et que des femmes)

*macro contenant les paramètres communs aux graphs
global marginopts " xlabel(-10 "50" -5 "55" 0 "60" 5 "65"  10 "70" 15 "75 y.") xtitle(Age) nodraw  scheme(s1mono)  yline(0, lcolor(gray)) recast(line)  legend(rows(1) subtitle("Education"))"

*macros contenant les paramètres spécifiques à chaque axe
local opt1 "ytitle("Dim.1 Convenience")  ytick(-40(10)20) ylabel(-40(10)20) xtitle("")  "
local opt2 "ytitle("Dim.2 Health")   ytick(-40(10)20) ylabel(-40(10)20) title("") xtitle("") "  // 
local  opt3 "ytitle("Dim.3 Tradition")  ytick(-40(10)20) ylabel(-40(10)20) title("") "

*pour étiquetage des deux colonnes
local sex1 "Women"
local sex0 "Men"
forvalues s = 0/1 {
di "`sex`s''"
}

*modèle et graphes des valeurs prédites
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yageC c.yageC#i.diplom3 i.diplom3 i.generation if ///
			femme==`s'  &	yage>49  ///
			|| proj_gest_ntt : 

		eststo AX`v'F`s'

		*estat icc
				quietly margins i.diplom3, at((base) _factor yageC = (-10(5)15) )
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot,  title("`sex`s''") ///
			name(gAX`v'F`s', replace)  /// chaque graph est nommé, pas sauvé
			$marginopts `opt`v''
	}
}

*graphique combiné
grc1leg 		///
	gAX1F0	///
	gAX1F1	///	
	gAX2F0	///
	gAX2F1	///
	gAX3F0	///
	gAX3F1	///
		, span scheme(s1mono) cols(2)  scale(0.5) iscale(1) xcommon ycommon
		
graph display, ysize(7) xsize(5) // permet de contrôler le rapport hauteur x largeur
graph export "$res\10_AGT_modeles_graphcombine.png", replace 
graph export "$res\10_AGT_modeles_graphcombine.ps", replace 
graph export "$res\10_AGT_modeles_graphcombine.tif", replace width(5000)

* tableau des  résultats 
esttab AX*F* using "$res\10_AGT_modelresults.txt", ///
	title("Changes in eating over age and across sex and educational level") ///
	not label mtitles(H1 H2 H3 F1 F2 F3) b(a2) nobaselevels replace tab


*================	Modele avec composition ménage 	===============	
	
** Le même, en rajoutant la composition du ménage, qui varie au fil du temps (time-dpendent)
* l'effet du couple et des autres membres du ménage est différent et aspire une partie
* de l'effet de l'âge sur l'axe 1.
* je ne génère pas de graphique

forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' c.yageC c.yageC#i.diplom3 i.diplom3  ib1.ycouple ib1.yenf01 i.generation ///
			if femme==`s' &	yage>49  ///
			|| proj_gest_ntt : 
	
		eststo BX`v'F`s'
		estat icc
	}
}

* tableau
esttab BX*F* using "$res\10_AGT_modelresults.txt", ///
	title("Modeling changes in eating over age and accounting for household structure") ///
	not label mtitles(H1 H2 H3 F1 F2 F3) b(a2) nobaselevels append tab

* ==========	 récpuérer les corrélations intraclasse 	==========
file open ICC using "$res\10_AGT_modeles_icc.txt", write replace

foreach model in AX1F0 AX2F0 AX3F0 AX1F1 AX2F1 AX3F1 BX1F0 BX2F0 BX3F0 BX1F1 BX2F1 BX3F1 {	
 estimates restore  `model'
 estat icc
 file write ICC "`model' ; 0" (r(icc2)) _n
 }

file close ICC

log close
exit


