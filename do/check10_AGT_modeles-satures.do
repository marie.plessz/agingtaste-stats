

use "$cree\9_AGT_axes.dta", clear

keep proj_gest_ntt a aqrep femme a98axe* diplom3 yage* generation ycouple yenf01
	   

** modèle avec âge saturé

			
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' i.yage##i.diplom3  if ///
			femme==`s'  &	yage>49  ///
			|| proj_gest_ntt : 
			eststo CX`v'F`s'
	}
}


*macro contenant les paramètres communs aux graphs
global marginopts " xtitle(Age) nodraw  scheme(s1mono)  yline(0, lcolor(gray)) recast(line)  legend(rows(1))"

*macros contenant les paramètres spécifiques à chaque axe
local opt1 "ytitle("Convenience")  ytick(-40(10)10) ylabel(-40(10)10) xtitle("")  "
local opt2 "ytitle("Health")  ytick(-10(10)20) ylabel(-10(10)20) title("") xtitle("") "  // 
local  opt3 "ytitle("Tradition")  ytick(-10(10)20) ylabel(-10(10)20) title("") "

*pour étiquetage des deux colonnes
local sex1 "Women"
local sex0 "Men"
forvalues s = 0/1 {
	di "`sex`s''"
	}
	
forvalues s=0/1 {
	forvalues v=1/3 {
	estimates restore CX`v'F`s'
		quietly margins i.diplom3, at((base) _factor yage = (50(1)75) ) 
			/* valeur de référence pour toutes les autres variables */
		quietly marginsplot, noci title("`sex`s''") ///
			name(gCX`v'F`s', replace)  /// chaque graph est nommé, pas sauvé
			$marginopts `opt`v''
	}	
}

*graphique combiné
grc1leg 		///
	gCX1F0	///
	gCX1F1	///	
	gCX2F0	///
	gCX2F1	///
	gCX3F0	///
	gCX3F1	///
		, span scheme(s1mono) cols(2)  scale(0.5) iscale(1) xcommon 
		
graph display, ysize(5) xsize(5) // permet de contrôler le rapport hauteur x largeur
graph export "$res\check10_AGT_modeles-satures.png", replace 
