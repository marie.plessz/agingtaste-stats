local tag "R2check_8_cree_AGT_inclus"
cap log close
log using "$res/log_`tag'.txt", text replace
/********************************************************************************
*	program:  8_cree_inclus
*	project: AGT
*	author: MP
* 	sélection des individus

* nb avant, avec les fichiers de charlotte :  9,976  inclus (hors ycouple et ynenf)
* elle excluait les individus partis à la retraite jusqu'à 50 ans inclus, ce qui me paraît
*inutile dans le travail actuel.

* R1 : isolated individuals out of sample due to attrition (death or exit). study population
*does not change.

*R2 : in order to check how mortality affected the results: 

- do not exclude individuals who died or quit

- total-non response: only exclude individuals who did not return the 1998 questionnaire
(required for the computation of the initial scores). do not exclude others (otherwise, 
dead participants are excluded)

- partial non-response: do not exclude respondents for which non-response could not be imputed in 1998 (same reason).

this makes the sample quite difficult to describes, with a non-balanced panel, but 
mixed linear models are robust to partial MAR non-response.




********************************************************************************/

use  "$cree\02_cree_base_wide_acm.dta", clear


count 
*==> 20625

******* R1 : Attrition : number of people out of sample because of exit or death ****
* sortieraison comes from 0a_cree_ind01pop_v3.do computes exits before specific date.

tab sortieraison  

/*
	
     Raison sortie enquête |
               <01/01/2014 |      Freq.     Percent        Cum.
---------------------------+-----------------------------------
                  0_aucune |     18,042       87.48       87.48
                  1_décédé |      1,965        9.53       97.00
2_départ EDFGDF ou abandon |        618        3.00      100.00
---------------------------+-----------------------------------
                     Total |     20,625      100.00
*/

* keep if sortieraison == 0  

* ==> do not exclude dead and quit

*(2,583 observations deleted)

count
* =>   18,042


 di "Attrition rate : "  %5.2f  (1- (r(N) /20625))*100 " %"
* ==> Taux d'attrition dû à exit ou décès  :  12.52 %

***** non-response to questionnaires which included food *****

keep if  aqrep1998==1  // & aqrep2004==1 & aqrep2009==1 & aqrep2014==1

* ==> only non-response in 1998 is a criteria of exclusion

*(7,120 observations deleted)

count
*==> 10922 

*****  Missing food info could not be imputed  *****

keep if missimp1998==1 // & missimp2004==1 & missimp2009==1 & missimp2014==1

* ==> only if partial non-response could not be imputed in 1998 is a criteria of exclusion

count
*==> 10575 (347 observations deleted)
* 347 individus n'ont pas pu être imputés


***** Not enough info for number of children ****

drop if missing(ynenf1998, ynenf2004, ynenf2009, ynenf2014)

count 
*==>   10,425 (150 observations deleted)
*==> new count :  14,368

misstable sum  diplom3 femme generation yage* ynenf* ycouple* 
* (variables nonmissing or string)


* ancienne variable
* generate inclu=(aqrep1998==1 & aqrep2004==1 & aqrep2009==1 & aqrep2014==1) & jage>50 &(missimp1998==1 & missimp2004==1 & missimp2009==1 & missimp2014==1)


reshape long aqannee aqdate ffqn ffq1 aqrep flag_ligne complet missimp mis3  yage6 ///
	yage ynper yactif  yactif2 retraite yr ffqnj  ycouple ynenf yconj_actif ev_retconjoint ///
	ycpdom ycptrav  yregime regime dejdom nbpdj nbrm nbrs collam collpm collngt ///
	viande volaille charcuterie poisson oeuf friture feculent legumect crudite lait ///
	laitage fromage dessert fruit pain viennoiserie sucre boissonsucree cafe vin light ///
	beurre huile matieregrasse epouse  yimc ysurpoids poidscorr dexdate pdfdate ///
	retlieudej ydejdom dd  mca_viande_  mca_volaille_  mca_poisson_  mca_oeuf_ ///
	mca_charcuterie_  mca_friture_  mca_legume_ mca_feculent_ mca_pain_ mca_light_ ///
	mca_cafe_ mca_boissonsucree_ mca_viennoiserie_ mca_crudite_ mca_fruit_ mca_laitage_ ///
	mca_lait_ mca_fromage_ mca_dessert_ mca_sucre_ mca_vin_  mca_matgrasse_ ///
		,i( proj_gest_ntt) j (a "1998" "2004" "2009" "2014")

count	
*==>   41,700 

* ==> new count :   57,472

misstable sum  viande volaille charcuterie poisson oeuf friture feculent legumect crudite lait ///
	laitage fromage dessert fruit pain viennoiserie sucre boissonsucree cafe vin light ///
	matieregrasse diplom3 generation femme  diplom3 ynenf ycouple
	
* (variables nonmissing or string)


***FIN
compress
note _dta : inclus pour ACM et modèles : individus = 10425, lignes = 41,700.
save "$cree\R2check_8_AGT_inclus.dta", replace

log close

exit
