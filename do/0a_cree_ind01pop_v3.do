********************************************************************************
// program: cree_ind01pop_v3
// task: preparation variable du fichier POP
// project: ALV
// author: MP le 19fev2014
// Adaptation du fichier de MP par CD le 20mai2015
*v3 : corrigé jage
********************************************************************************

local tag "cree_ind01pop_v3 CD 2015-05-20"

// #1
// cree le fichier ind01pop
use "$source\0_DATA_POP_fev2015.dta" , clear
save "$temp\t_ind01pop.dta", replace 
note: selection et creation de variables sur le fichier source 0_DATA_POP_fev2015.dta, `tag'
// #2
// drop
drop mort_cses_cim mort_cses_dcprinc mort_cses_libprinc

// #3
// creation variables
*age retraite
gen  retdate=date(suivi_pop_retdate, "DMYhm" )
format retdate %d
gen datnais=date(suivi_pop_datnais, "DMYhm")
format retdate %d
gen double jage= year( retdate - datnais) - 1960
	label var jage "Age au départ en retraite"
	note jage: age révolu / `tag'
*Toutes les var liées au moment de la retraite commencent par J

*age retraite en caté
recode jage (min/54=54)(55=55)(56/max=56) , gen(jagecat)
label var jagecat "Age départ retraite"
label def jagecat 54 "Avant55 ans" 55 "55 ans" 56 "Après 55 ans"
label val jagecat jagecat
note jagecat: recodage en tranches de jage. 55 ans est le départ légal pour les ///
	pers sur poste actif (60 ans pour les postes pas actifs). `tag'

*verif raisons sortie
gen dexdate=date(suivi_pop_dexdate, "DMYhm")
format dexdate %d
gen pdfdate=date(suivi_pop_pdfdate, "DMYhm")
format pdfdate %d
gen dcddate=date(suivi_pop_dcddate, "DMYhm")
format dcddate %d
gen ab=(dexdate  < date("01/01/2014", "DMY"))
gen edf=(pdfdate < date("01/01/2014", "DMY"))
gen dcd=(dcddate < date("01/01/2014", "DMY"))

*==>il y a des dates de décès y compris pour les abandons et les sortis d'EDF
local vardrop " ab edf dcd "  //ramasse les var que je ne garderai pas dans pop_01

*raison sortie
gen sortieraison=0
	replace sortieraison=1 if dcd == 1
	replace sortieraison=2 if ab == 1 | edf == 1

	label var sortieraison "Raison sortie enquête <01/01/2014"
	label def sortieraison 0 "0_aucune" 1 "1_décédé" 2 "2_départ EDFGDF ou abandon" 
	label value sortieraison sortieraison
	note sortieraison: `tag'

*date sortie
gen sortiedate double =dcddate
	replace sortiedate=dexdate if dexdate<dcddate
	replace sortiedate=pdfdate if pdfdate<dcddate & pdfdate<dexdate
	format sortiedate %td
	label var sortiedate "Date sortie enquête"
	note sortiedate: `tag'

//	#4 donner noms sympas aux var utiles	

*année naissance
gen annais= year(datnais)
label var annais "Année de naissance"
note annais: `tag'

/*date naissance
rename suivi_pop_datnais datnais
*/
*generation/cohorte de naissance : NEAP45
gen neap45=irecode(annais, 1945)
label var neap45 "Né(e) après 1945"
label val neap45 Lon01
note neap45: cohorte ou génération de naissance. J'ai coupé à l'année ///
	qui maintient le mieux l'équilibre hommes/femmes / `tag'
	
* Lieu de naissance 

label variable gpso_adm_comnais "Commune de naissance"
rename gpso_adm_comnais comnais
label variable gpso_adm_depnais  "Département de naissance"
rename gpso_adm_depnais depnais
*FEMME
gen femme=suivi_pop_sexe==2
label var femme "Femme"
label def Lon01 0 "Non" 1 "Oui", modify
label def femme 0 "Homme" 1 "Femme", modify
label value  femme femme
note femme: "indicatrice pour les femmes (minoritaires) `tag'
	
*date exacte retraite
clonevar retdate2=suivi_pop_retdate
note retdate2: copie de suivi_pop_retdate / `tag'
	
drop `vardrop'
drop suivi_pop_* retdate2
sort proj_gest_ntt
compress
save "$temp\t_ind01pop.dta", replace 



