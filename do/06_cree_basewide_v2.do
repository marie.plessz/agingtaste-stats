********************************************************************************
* Program  :cree_reshape
* Project : Thèse
* Author : CD
* Date : 16Oct2014
* Task : 
/* Créer une base en format wide à partir de la base fusionnée et nettoyée
objectif : créer une variable inclus avec les critères d'inclusion de la 
population pour les femmes
*/
********************************************************************************
use "$cree\01_cree_base.dta", clear

/*
* Probleme avec l'année de la retraite : elle change au cours du temps!!! (à voir dans les dofiles de marie par la suite, pour l'instant correction)
*/

sort i a
gen retan = retannee if a==1989 
bys i : carryforward retan , replace
drop retannee
rename retan retannee

sort i a
gen retdat = retdate if a==1989 
bys i : carryforward retdat , replace
format retdat %td
drop retdate
rename retdat retdate

recode depnais (.=9999)
sort i a
gen depn = depnais if a==1989 
bys i : carryforward depn , replace
drop depnais
rename depn depnais

recode comnais (.=9999)
sort i a
gen comn = comnais if a==1989 
bys i : carryforward comn , replace
drop comnais
rename comn comnais

sort i a 
bys i : carryforward dcddate , replace
recode dcddate (.=0)
label define dcd 0 "Non décédé"
label value dcddate dcd

recode taille90 (.=9999)
sort i a
gen t90 = taille90 if a==1989
bys i : carryforward t90 , replace
drop taille90
rename t90 taille90

drop yetatscat ytnerfacat ytphyfacat ytnerfa ytphyfa yphyfatcat ynerfat yetats yphyfat 


keep if a==1998|a==2004|a==2009 |a==2014

reshape wide aqannee aqdate ffqn ffq1 aqrep flag_ligne complet missimp mis3 ///
	yage6 yage ynper yactif  yactif2 retraite yr ffqnj  ycouple ynenf yconj_actif ev_retconjoint ///
	ycpdom ycptrav  yregime regime dejdom nbpdj nbrm nbrs collam collpm collngt ///
	viande volaille charcuterie poisson oeuf friture feculent legumect crudite lait ///
	laitage fromage dessert fruit pain viennoiserie sucre boissonsucree cafe vin light ///
	beurre huile matieregrasse  epouse  yimc ysurpoids poidscorr dexdate pdfdate ///
	retlieudej ydejdom dd , i(proj_gest_ntt) j (a)
/* retiré  ydismarch ynbrvufam  ttrajet visite assosport assoprosynd partipol 
assoenfant assohuman groupautre caritative maladehandic rdservice clubsport religieux formation politique autreact
 */

egen nbaqrep=rowtotal(aqrep1998 aqrep2004 aqrep2009 aqrep2014)
label variable nbaqrep " Nombre de réponses aux AQ après 1990"

/*
* Creation d'une variable " a répondu à au moins 2 AQ"
generate nbaqrep2=(nbaqrep>=2) if nbaqrep!=.
label variable nbaqrep2 " A répondu à au moins 2 AQ"
label define nbaqrep2 0 "Moins de 2 AQ" 1 " Au moins 2 AQ"
*/



save "$cree\02_cree_base_wide.dta", replace

