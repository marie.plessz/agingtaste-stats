********************************************************************************
*	program:	3 cree_ALV02recode_v5.do
*	project:	ALV
*	author:		MP (&CD)
*	date: 		8jan2014
*   Adaptation du fichier de MP par CD pour intégrer les données 2014. 
/*	task:
recodages sur RL01
crée le fichier RL02 sur lequel je travaillerai
pas de fichier wide
pas de codebook
variables at baseline (bl) pour des imputations multiples le plus efficace possible
v4:
	ajouté jetatscat et jimc3, recodages en classe de la santé perçue et de l'imc.
	l'idée c'est d'essayer de les imputer en utilisant les valeurs at baseline
	créé etatsblcat, recodé en 3 caté pour imputer c'est bien suffisant
v5 : var retraite, mvsesante, epouse
*/
local tag "3 cree_ALV02recode_v5.do cd 20mai2015"
********************************************************************************

use "$cree\ALV01", clear

//	#1
//	recodages variables de contrôle etc

*age à chaque aq
/*gen yage=year(aqdate-datnais) - 1960
replace yage=year(mdy(1,1,aqannee) -datnais) - 1960 if aqdate==.
label var yage "Age"
note yage: aqdate-datnais en années / `tag'
*/

*je préfère un âge arrondi qui augmente d'un an chaque année
gen yage=aqannee-annais
label var yage "Age"
note yage : aqannee - annais / `tag'


gen vague=.
bys i (a): replace vague=0 if yr[1]> -2  & yr[1]<.
bys i (a): replace vague=1 if yr[1]> -10 & yr[1]<=-2 
bys i (a): replace vague=2 if yr[1]> -16 & yr[1]<= -10
bys i (a): replace vague=3 if yr[1]> -21 & yr[1]<= -16
bys i (a): replace vague=4 if yr[1]> -26 & yr[1]<= -21
bys i (a): replace vague=5 if 			   yr[1]<= -26 

label var vague "Vague départ retraite"
label def vague 0 "Avt AQ1990" 1 "AQ1990-AQ1998" ///
	2 "AQ1998-AQ2004" 3 "AQ2004-AQ2009" 4"AQ2009-2014" 5 "Apr AQ2009", modify
label value vague vague
note vague: je m'appuie sur la valeur de yr en 1989 pour ce codage. / `tag'

* retraité ou pas : 
gen retraite= yr>=0
replace retraite=. if yr==.
label var retraite "Retraité"
label val retraite Lon01
note retraite : retraite= yr>=0. indicatrice de si le répondant est retraité ou non. `tag'

*interaction retraite ydejdom
gen retlieudej=ydejdom +1
		replace retlieudej=0 if retraite==1
label var retlieudej "Lieu du déjeuner ou retraite"
label def retlieudej 1 "Déjeune exterieur et actif" 2 "Déjeune dom et actif" 0 "Retraité", modify
label val retlieudej retlieudej
note retlieudej: vaut ydejdom si la pers n'est pas retraitée (retraite==0),	///
	vaut 1 si actif ydejdom=0, vaut 2 si actif et ydejdom==1 (déjeune à domicile). `tag'


* situation de l'épouse
gen s=statutcjtbl*10
gen q=yconj_actif
recode q(2=7)
gen epouse=s+q
recode epouse (0 10 20 30 70= 0) (1 7 =9) (31=21) (37=27) 
replace epouse=. if femme==1
replace epouse=9 if statutcjtbl==0 & ycouple==1 & yconj_actif==.
label var epouse "Epouse en 89 et au moment du FFQ"
label def epouse 0 "Pas d'épouse"  9 "Mise en couple" ///
	11 "Bas active" 17 "Bas inactive" ///
	21 "Moy/sup active" 27 "Inact moy/sup" ///
	71 "Reprise act" 77 "Tjs inactive", modify
label val epouse epouse
drop s q
note epouse: recode de statutcjtbl et yconj_actif seult pour les hommes. 	///
	regroupement de modalités de façon à ne pas mélanger des catégories 	///
	nombreuses et intéressantes avec des catégories rares. regroupement ///
	des statuts professionnels 	moyens/sup pour contraste avec bas ///
	(ouvrier/employée). 0 regroupe les hommes célibataires 	en 89 et au ///
	moment du FFQ ac les pers qui avaient une épouse en 89 mais n'en ont pas  ///
	au moment du ffq. `tag'
	
	
* FFQNJ position du ffq par rapport à date de retraite.
/*ffqnj doit renvoyer au N° de l'année même si l'AQ n'est pas rempli 
sinon on aura des pb quand on fera les imputations.
*/

gen ffqnj=0-vague if a==1990
replace ffqnj=1-vague if a==1998
replace ffqnj=2-vague if a==2004
replace ffqnj=3-vague if a==2009
label var ffqnj "Rang ffq % retraite"
note ffqnj: 0 premier ffq après retraite. ffq est numéroté même si NR. / `tag'

*AQREP a des valeurs manquantes uniquement pcq'il manque des lignes dans le fichier des AQ:
recode aqrep(.=0)
note aqrep: quand aucun questionnaire n'est envoyé, il n'y a pas de ligne dans le fichier des AQ. ///
	ça crée une val manquante dans RL. je recode les manquantes en 0 (n'a pas renvoyé d'AQ). `tag'
	
//	2
//	recodages vars sociodémo
*diplome
recode diplom (1 2 4 5=0)(3 6 7 8=1) ,gen(bacouplus)
label val bacouplus Lon01
label var bacouplus "Bac ou études sup"
note bacouplus: 'autre diplome' recodé avec bac et études supérieures / `tag'
	
recode diplom (1 2=1 "BEPC") (4 5=2 "CAP BEP") (3 6 7 8=3 "Bac et +") (. = 1), gen(diplom3)
label var diplom3 "Diplôme 3 catégories"
note diplom3: 'autre diplome' recodé avec bac et études supérieures / `tag'

*generation
gen generation=irecode(annais, 1943, 1948)
label var generation "Génération de naissance"
label def generation 0 "1939-1943" 1 "1944-1948" 2 "1949-1953 (femmes)", modify
label val generation generation
note generation: 3 tranches d'env 5 ans, la dernière que des femmes. ///
	cf ZINS 2010 sur alcool / `tag'

*classes d'âge pour ffq
gen yage6=irecode(yage,39,45,51,57,63,69)
label def yage6 0 "35-39" 1 "40-45" 2 "46-51" 3 "52-57" 4 "58-63" 5 "64-69" 6 "70-74", modify
label val yage6 yage6
note yage6: recode de l'âge en tranches de 6 ans. bien pour travailler sur les ffq / `tag'
	
//	3
/*	recodages divers
bys i (a): egen d= total(lgm14<.)
gen misslgm=4-d
label var misslgm "Nb de NR à item légumes"
note misslgm: nombre de valeur manquantes aux items legumes verts ou cuits pour cet individu / `tag'
drop d
*/
*etat de santé et fatigue en catégories
label def Lfatcat 1 "1-3 Non" 2 "4-5 Moyen" 3 "6-8 Très", modify
foreach var of varlist ynerfat yphyfat ytnerfa ytphyfa {
	recode `var' (1/3=1)(4 5=2)(6/8=3), gen(`var'cat)
	label value `var'cat Lfatcat
	note `var'cat: recode de `var' en catégories, `tag'
	note `var': voir aussi variable catégorielle `var'cat (`tag')
	}	
label var ynerfatcat "Fatigué nerveusement"	
label var yphyfatcat "Fatigué physiquement"
label var ytphyfacat "Travail physiquement fatiguant"
label var ytnerfacat "Travail nerveusement fatiguant"

recode yetats (1/3=1)(4 5=2)(6/8=3), gen(yetatscat)
label def yetatscat  1 "1-3 Bon" 2 "4-5 Moyen" 3 "6-8 Mauvais", modify
label value yetatscat yetatscat
note yetatscat: version en catégories de yetats / `tag'
note yetats: voir aussi variable catégorielle yetatscat (`tag')

*yactif en dichotomique
recode yactif (1 4=1)(3 5=0), gen(yactif2)
label var yactif2 "Travaille (AQ)"
label val yactif2 Lon01
note yactif2: version dichotomique de yactif, `tag'

*corpulence
gen ysurpoids=yimc>25
replace ysurpoids=. if yimc==.
label var ysurpoids "IMC>25"
label val ysurpoids Lon01
note ysurpoids: recodage de yimc, yimc>25. `tag'

//	5
//	valeurs "at baseline" cad 
*la 1° année où Q° est posée, si manquante la suivante, si manquante la suivante.
*IMCBL et IMCCLBL
bys i (a) : egen imc90=sum(yimc*(a==1990))
bys i (a) : egen imc91=sum(yimc*(a==1991))
bys i (a) : egen imc92=sum(yimc*(a==1992))
recode imc90 imc91 imc92 (0=.)

gen imc9092=imc90
replace imc9092=imc91 if imc90==.
replace imc9092=imc92 if imc90==. & imc91==.
label var imc9092 "IMC at baseline (AQ)"
note imc9092: IMC en 1990 (1° AQ). si manquant je regarde 1991, si manquant aussi je regarde 1992. `tag'

gen imcclbl=1 if imc9092<18.5
	replace imcclbl=2 if imc9092>=18.5 & imc9092<25 	
	replace imcclbl=3 if imc9092>=25 & imc9092<30 	
	replace imcclbl=4 if imc9092>=30 & imc9092<35 	
	replace imcclbl=5 if imc9092>=35 & imc9092<40 	
	replace imcclbl=6 if imc9092>=40 & imc9092!=.
	recode imcclbl (6=5) // je regroupe l'obésité 2 et 3 car petits effectifs
	label var imcclbl "classe imc at baseline (AQ)"
	*label define imc 1"underweight" 2"normal weight" 3"over weight" 4"class I obesity" 5"class II obesity" 6"class III obesity"
	label define imc2 1"underweight" 2"normal weight" 3"over weight" 4"class I obesity" 5"class II/III obesity",modify
	label values imcclbl imc2
note imcclbl: classes de corpulence d'après AQ1990. si poids manquant en 1990 on regarde 1991 et 1992. ///
	je regroupe imc (35-40) et imc>40. `tag'
	
//	6
// imc et santé 1 an avant la retraite
		// etats avant retraite
bys i (a)  : egen jetatscat=sum(yetatscat*(yr==-1))	
recode jetatscat (0=.)
label var jetatscat "Santé perçue 1 an avt retraite"
label value jetatscat yetatscat
note jetatscat: valeur de yetatscat pour yr=-1.

		// imc avant retraite
bys i (a)  : egen jimc=sum(yimc*(yr==-1))	
recode jimc (0=.) 
gen jimc3=2 if jimc<25 	
replace jimc3=3 if jimc>=25 & jimc<30 	
replace jimc3=4 if jimc>=30 & jimc<.
label var jimc3 "IMC 1 an avt retraite"
label def Limc3 2 "2_IMC<25" 3 "3_IMC [25;30[" 4 "4_IMC>=30"
label value jimc3 Limc3
note jimc3: valeur de yimc pour yr=-1. recodé en 3 classes : sous-poids ou normal (<25) ///
	surpoids ([25;30[) et obésité (>=30).

drop imc90 imc91 imc92 jimc

*ETATSBL
bys i (a) : egen etatscat89=sum(yetatscat*(a==1989))
recode etatscat89 (0=.)
label var etatscat89 "Etat de santé en 89"
label val etatscat89 yetatscat
note etatscat89: valeur de yetatscat en 1989 `tag'

bys i (a) : egen etats89=sum(yetats*(a==1989))
bys i (a) : egen etats90=sum(yetats*(a==1990))
bys i (a) : egen etats91=sum(yetats*(a==1991))
recode etats89 etats90 etats91 (0=.)

gen etatsbl=etats89
replace etatsbl=etats90 if etats89==.
replace etatsbl=etats91 if etats90==. & etats89==.
recode etatsbl(7 8=7)
label def etatsbl 1 "1 très bon" 7 "7 ou 8 très mauvais"
label val etatsbl etatsbl
label var etatsbl "Santé perçue at baseline (AQ)"
note etatsbl: Santé perçue en 1989 (1° AQ). si manquant je regarde 1990, si manquant aussi je regarde 1991. ///
	je regroupe 7 et 8. `tag'
drop etats89-etats91

recode etatsbl (1/3=1)(4 5=2)(6/8=3), gen(etatsblcat)
label var etatsblcat "Santé perçue at baseline"
label val etatsblcat yetatscat
note etatsblcat: recode de etatsbl en 3 classes (1/3=1)(4 5=2)(6/8=3) , `tag'

gen mvsesantebl=etatsblcat
recode mvsesantebl (1=0) (2 3=1)
label var mvsesantebl "Mauvaise santé perçue (>3, 1 tb/8tmvs)"
label val mvsesantebl Lon01
note mvsesantebl: recode de etatsblcat dichotomique. bonne santé : 1à 3; ///
	 mauvaise sante: 4 à 8. moins déséquilibré que codage en 3 caté. `tag'
	 
*en couple at baseline
bys i (a) : egen couple89=sum((ycouple+1)*(a==1989))
bys i (a) : egen couple90=sum((ycouple+1)*(a==1990))
bys i (a) : egen couple91=sum((ycouple+1)*(a==1991))
recode couple89-couple91 (0=.)
gen couplebl=couple89 -1
replace couplebl=couple90 -1 if couple89==.
replace couplebl=couple91 -1 if couple90==. & couple89==.
label var couplebl "En couple at baseline (AQ)"
label val couplebl Lon01
note couplebl: en couple en 1989 (1° AQ). si manquant je regarde 1990, si manquant aussi je regarde 1991. `tag'
drop couple89-couple91

*corriger statutcjtbl par rapport à couplebl
replace statutcjtbl=. if couplebl==.
note statutcjt: manquante si on n'a pas d'info sur le statut conjugal en 89,90 ou 91 cad si couplebl==./ `tag'

	// IMC at baseline
		//v3 plus regroupée
recode imcclbl (1=2) (5=4)  , gen(imcbl3)	
label var imcbl3 "IMC at baseline 3 groupes"
label value imcbl3 Limc3
note imcbl3: recodage de imcclbl en 3 groupes pour mieux imputer car version plus détaillée ///
	entraîne des catégories à très faibles effectifs (obésité type 3 + chez les hommes ///
	le sous-poids). `tag'

*classes d'âge de retraite un peu plus détaillées que jagecat
gen jagecatdet=.
replace jagecatdet=50 if jage<=50
replace jagecatdet=jage if jage>50 & jage<=60
replace jagecatdet=61 if jage>60 & jage<.
label def jagecatdet 50 "50 ans ou moins" 61 ">60 ans"
label val jagecatdet jagecatdet
label var jagecatdet "Age retraite (50-60)"
note jagecatdet: recode de jagecat en regroupant 50 et moins et 61 ans et plus pour IM. `tag'

//	4
//	Nettoyer sauver
order i a yr  retraite ffqnj ffq1 ffqn aqrep aqannee aqdate retannee retdate ///
	invlmmp invdate  vague ///
	neap45 generation annais datnais ///
	femme diplom bacouplus diplom3 ///
	nbhab_92 fumeur89 alcool89 dejdom89 etatscat89 jagecatdet ///
	imcclbl imcbl3 couplebl mvsesantebl etatsbl etatsblcat ///
	revenu_89 revenu_02 ///
	revpc_89 revpc_02  ///
	proouv proper conj_pro_89 ev_retconjoint ///
	jage jagecat gf1_entree jgf1 gf1_mob gf1_35 ///
	pcs_entree pcs_35 jpcs ///
	sortieraison sortiedate flag_ligne yage6 yage ysurpoids ///
	ynerfatcat  yphyfatcat  ytnerfacat  ytphyfacat  yetatscat yactif2 ycouple ynenf ///
	yregime retlieudej ydejdom
compress
note _dta: fichier RL01 + les créations de variables y compris valeur 'at baseline' ///
	cad en regardant valeur les 2 années suivantes pour boucher un max de trous pour les IM. `tag'
label data "RL02 : Fichier format long avec tous les recodages"
save "$cree\ALV02", replace


