local tag "R2check_09_AGT_axes"
cap log close
log using "$res/log_`tag'.txt", text replace

use "$cree\R2check_8_AGT_inclus.dta", clear

 /* les vars mca_blabla sont recodées comme ceci : les modalités qui ont moins de ///
	5% des effectifs sont regroupées avec la modalité adjacente.
 les manquantes ont été imputées si indv a répondu à au moins 14 items */
 
/* R1 : ajouté scree plot
*/

* comprendre le codage des vars mca


	

foreach X in viande volaille charcuterie poisson oeuf friture feculent legume crudite lait ///
	laitage fromage dessert fruit pain viennoiserie sucre boissonsucree cafe vin light ///
	  {
		tab mca_`X'_ `X'
		}
tab mca_matgrasse_
	
mca mca_*_ if a ==1998 , dimension (3) compact method(indicator) normal(princ) suppl(femme generation diplom3)

**** R1 - screeplot  ****

screeplot

* Not beautiful enough for me. R has made me picky
*save the pct of inertia
matrix M = e(inertia_e)'
svmat M
gen dim = _n if _n<44

*graph
graph twoway bar M1 dim, ytitle("Inertia (%)") scheme( s2mono) ///
	xtitle(Dimension)  graphregion(color(white))  

graph display, ysize(4) xsize(8)
graph export 	"$res/R2check_09_screeplot.png", replace

* results : mass and coordinates
matrix list e(cGS) 

* results : contributions computed as in R, SAS or SPAD
mcacontrib

* save axes
predict axe1 axe2 axe3  

sum  axe1 axe2 axe3  

/*
pour vérifier que résultats comparables à version précédente
save "$temp/t_test.dta", replace
merge proj_gest_ntt a using "$cree\04_cree_base_long_complet.dta"
corr axe* a98axe*
*/

* on change le signe des axes  pour que l'interprétation soit plus naturelle
gen a98axe1=axe1*(-100)
gen a98axe2=axe2*(-100)
gen a98axe3=axe3*(-100)

/*replace a98axe1= . if inclu==0
replace a98axe2= . if inclu==0
replace a98axe3= . if inclu==0
*/

label variable a98axe1 "Convenience"
label variable a98axe2 "Health"
label variable a98axe3 "Tradition"
drop axe1 axe2 axe3

***** R1 - Scatter plot of individuals ***********

scatter  a98axe2 a98axe1 if a ==1998, msymbol(+) legend(label(1 "1998 (active)" )) || scatter  a98axe2 a98axe1 ///
	if a ==2014,  ///
	msymbol(oh) legend(label(2 "2014 (supplementary)")) aspectratio(1) ///
	xtitle(Dimension 1 Convenience) ytitle(Dimension 2 Health) ///
	scheme(s2mono) xline(0) yline(0) graphregion(color(white))  note(Coordinates multiplied by -100.)
	
graph export 	"$res/R2check_09_MCA_scatter_indv.png", replace

********** Dernières créations de variables avant modèle **********

*** Variable d'âge poru les modèles : 0 en 60

gen yageC = yage-60
label var yageC "Age"

* pour les modèles à rupture de pente
gen yage1B = yageC
replace yage1B= 0 if yage>60
*scatter  yage1B yage
*==> c'est bon, yage1B augmente de -15 à 0 avec 0 à partir de 60 ans

gen yage2B = yageC
replace yage2B= 0 if yage<60
*scatter  yage2B yage
*==> c'est bon, yage2B augmente de 0 à 14 ou 15 à partir de 60 ans


label def diplom3_en 1 "Primary" 2 "Secondary" 3 "Tertiary", modify
label val diplom3 diplom3_en	   

label def ycouple 0 "No partner" 1 "With partner", modify
label val ycouple ycouple


recode ynenf (0=0) (1/max = 1), gen(yenf01)
label var yenf01 "Lives with children"
label  def yenf01 0 "No child" 1 "With child(ren)", modify
label value yenf01 yenf01

save "$cree\R2check_09_AGT_axes.dta", replace


log close

exit

