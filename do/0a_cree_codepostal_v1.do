********************************************************************************
*	program:	cree_codepostal_v1.do
*	project:	ALV
*	author:		CD
*	date: 		20 Mai 2015
/*	task:
********************************************************************************
*/
use "$source\4_DATA_CP_fev2015", clear
save "$temp\t_codepostal.dta", replace
use "$temp\t_codepostal.dta", clear
rename suivi_annee aqannee
compress
sort proj_gest_ntt aqannee

save "$temp\t_codepostal.dta", replace
