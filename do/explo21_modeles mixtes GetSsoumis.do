local tag "21_modeles mixtes GetSsoumis.do"
cap log close
log using "$res/log_`tag'.txt", replace text

/*
MP le 15/05/2019
Sur la base du travail de charlotte, modèles présentés dans la version 
de l'article soumise à Gerontologie et société
voir fichier 21_modeles mixtes MP pour les différents essais de modèles.
*/

use "$cree\04b_cree_base_long_inclu.dta", clear
/* ==> extrait des données pour vérif de l'ACM sous R
export delimited  proj_gest_ntt aqannee missimp yage6 yage femme generation diplom3 ///
	gf1_35 retraite viande-matieregrasse inclu mca_* a98axe* ///
	using "ALV_1", replace delimiter (";")
*/


*=========>>>>>> Les modèles qu'on retient !!!!!!!!!!!!
** 6 modèles
forvalues s=0/1 {
	forvalues v=1/3 {	
		di "axe`v' Femme = `s' "
		mixed a98axe`v' yage1B yage2B i.diplom3 i.generation if femme==`s' & inclu==1  || proj_gest_ntt : 
		eststo X`v'F`s'
		estat icc
	}
}
set dp comma
esttab X*F*, not label mtitles(H1 H2 H3 F1 F2 F3) b(a2)


log close
