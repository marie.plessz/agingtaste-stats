* stats descriptives sur les données "wide"
use "$cree\02_cree_base_wide.dta",clear

* lieu + fréquent du déjeuner et diplôme après retraite 
 tab dejdom2009 diplom3 if retraite2009==1, col nof chi2
 
 /*
 
      2009 |       Diplôme 3 catégories
    dejdom |      BEPC    CAP BEP   Bac et + |     Total
-----------+---------------------------------+----------
       Non |      1.57       1.28       2.14 |      1.57 
       Oui |     98.43      98.72      97.86 |     98.43 
-----------+---------------------------------+----------
     Total |    100.00     100.00     100.00 |    100.00 

          Pearson chi2(2) =  11.1403   Pr = 0.004
*/

* lien entre diplôme et statut pro du père, selon le sexe 
bys femme : tab statutpere diplom3 , col nof chi2


/*
. bys femme : tab statutpere diplom3 , col nof chi2

----------------------------------------------------------------------------------------------------
-> femme = Homme

         Statut |
  professionnel |       Diplôme 3 catégories
        du père |      BEPC    CAP BEP   Bac et + |     Total
----------------+---------------------------------+----------
            Bas |     40.39      44.95      26.74 |     39.12 
          Moyen |     31.60      30.41      28.97 |     30.26 
           Haut |     17.93      15.81      36.26 |     21.77 
7_Autre/inconnu |     10.07       8.83       8.03 |      8.86 
----------------+---------------------------------+----------
          Total |    100.00     100.00     100.00 |    100.00 

          Pearson chi2(6) = 762.1508   Pr = 0.000

----------------------------------------------------------------------------------------------------
-> femme = Femme

         Statut |
  professionnel |       Diplôme 3 catégories
        du père |      BEPC    CAP BEP   Bac et + |     Total
----------------+---------------------------------+----------
            Bas |     38.14      39.84      22.47 |     35.26 
          Moyen |     28.66      30.58      30.48 |     30.03 
           Haut |     21.54      20.36      39.43 |     25.20 
7_Autre/inconnu |     11.65       9.23       7.62 |      9.51 
----------------+---------------------------------+----------
          Total |    100.00     100.00     100.00 |    100.00 

          Pearson chi2(6) = 226.1367   Pr = 0.000
*/
		  


/*export delimited  proj_gest_ntt aqannee missimp yage6 yage femme generation diplom3 ///
	gf1_35 retraite viande-matieregrasse inclu mca_* a98axe* ///
	using "ALV_1", replace delimiter (";")
*/

