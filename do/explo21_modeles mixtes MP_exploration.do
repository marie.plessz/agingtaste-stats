/*
MP le 11/04/2019
Sur la base du travail de charlotte, améliorer les modèles pour capter les différences sociales dans le vieillissement
*/



use "$cree\04b_cree_base_long_inclu.dta", clear

/*export delimited  proj_gest_ntt aqannee missimp yage6 yage femme generation diplom3 ///
	gf1_35 retraite viande-matieregrasse inclu mca_* a98axe* ///
	using "ALV_1", replace delimiter (";")
*/
	
*Variable qui capte un changement de pente à partir de la retraite (s'il est nul c que pas de changement de pente)
/* en fait la var existe déjà : yr */
	   gen yage_j = yage - jage
	   replace yage_j = 0 if yage_j < 0
	   label var yage_j "annees depuis retraite"

	   gen yage3 = yage - 60
	   replace yage3 = 0 if yage < 60
	   
	   
	   * #1 comparer un modèle avec rupture de pente à 60 ans (yage3) et avec rupture de pente à l'âge de la retraite
forvalues v=1/3 {	
	mixed a98axe`v' yage yage3  if femme==0 & inclu==1|| proj_gest_ntt : 
	eststo AGE`v'
	mixed a98axe`v' yage yage_j if femme==0 & inclu==1|| proj_gest_ntt : 
	eststo RET`v'
	esttab AGE`v' RET`v', not ci
}	   
* ==> les modèles avec rupture de pente à la retraite sont plus précis
	   
	   * #2 avec rupture de pente à retraite, ajouter le dipl, et un "palier" au moment de la retraite
forvalues v=1/3 {	
	mixed a98axe`v' yage yage_j i.diplom3 i.retraite if femme==1 & inclu==1|| proj_gest_ntt : 
	eststo FULL`v'
}
esttab FULL*, not ci

* ==> L'âge compte pour l'axe 1, le diplome pour l'axe 2, pour l'axe 3... le genre? 
* la retraite compte pour tout mais pas dans les mêmes proportions
	
	* est-ce que le vieillissement diffère par diplôme?
forvalues v=1/3 {	
	mixed a98axe3 yage yage_j if diplom==`v' & femme==0 & inclu==1|| proj_gest_ntt : 
	eststo AXE3_dip`v'
}
esttab AXE3_dip1 AXE3_dip2 AXE3_dip3 , not ci

* ==> NON ! on trouve la même chose.

* comparer les sexes / on enlève la retraite car trop de degré de liberté.
** avec des interactions
forvalues v=1/3 {	
	mixed a98axe`v' yage c.yage_j#i.femme  i.diplom3##1.femme if inclu==1|| proj_gest_ntt : 
	eststo FULL`v'
}
esttab FULL*, not ci

*=========>>>>>> Les modèles qu'on retient !!!!!!!!!!!!
** 6 modèles
forvalues s=0/1 {
	forvalues v=1/3 {	
		mixed a98axe`v' yage yage3 i.diplom3 i.generation if femme==`s' & inclu==1 & yage> 49|| proj_gest_ntt : 
		eststo X`v'F`s'
		estat icc
	}
}
esttab X*F*, not label mtitles(H1 H2 H3 F1 F2 F3)  eqlab("" "ln(random int.)" "ln(resid. error)", none) 


