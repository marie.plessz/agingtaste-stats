


use "$cree\02_cree_base_wide_acm.dta", clear


 list proj_gest_ntt  ynenf1998 ynenf2014 in 1/100 if ycouple1998 != ycouple2014

*      +--------------------------------+
*      | proj_g~t   yne~1998   yne~2014 |
*      |--------------------------------|
*  10. | 48100010          1          0 |
*  22. | 48100022          2          2 |
*  23. | 48100023          0          0 |
*  34. | 48100034          1          0 |
*  47. | 48100047          2          1 |
*      |--------------------------------|
*  53. | 48100053          2          0 |
*  60. | 48100060          1          0 |
*  84. | 48100084          0          0 |
*  86. | 48100086          1          0 |
*  91. | 48100091          2          0 |
*      |--------------------------------|
*  96. | 48100096          1          1 |
*  

* J'ai choisi  48100034    

use "$cree\9_AGT_axes.dta", clear

 list aqannee yage  ynenf ycouple yactif if proj_gest_ntt == 48100034
 /*
       | aqannee   yage   ynenf        ycouple                 yactif |
       |--------------------------------------------------------------|
   73. |    1998     54       1     No partner              En emploi |
   74. |    2004     60       0     No partner   Retraite préretraite |
   75. |    2009     65       0   With partner   Retraite préretraite |
   76. |    2014     70       0   With partner                      . |

   */
 
 list diplom femme  couplebl if proj_gest_ntt == 48100034
 
 /*
       | diplom   femme   couplebl |
       |---------------------------|
   73. |    CAP   Homme        Oui |

*/
