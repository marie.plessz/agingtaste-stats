/*
Author: MP
projet : AGT
TASK : check  the stability of the MCA performed in 1998 over time : 
would individuals had different coordinates if we  made a new ACM each year?

We run one MCA aper year and examine :
- whether the same variables contribute to the axes
-whether the individual coordinates derived from these MCAs (in which individuals 
	are active every year) correlate with the coordinates derived from the 1998 
	MCA (in which individuals are supplementary in 2004-2014).
*/


***** Check stabilité des axes ****

use "$cree\9_AGT_axes.dta", clear


cap erase "$res/check09_MCA_`an'_contributions.csv"

foreach an of numlist 1998 2004 2009 2014 { 
	mca mca_*_ if a ==`an' , dimension (3) compact method(indicator) normal(princ)/*suppl(pcs98 pcs04 pcs09)*/
	matrix M`an' = e(inertia_e)'
	matrix N =   e(cGS)
	mcacontrib
	matrix Res`an' =N , NewContrib
	mat2txt , matrix(Res1998) saving("$res/check09_MCA_`an'_contributions.csv") append

	predict Ca`an'axe1 Ca`an'axe2 Ca`an'axe3  if a ==`an'
}

/* tableau wide avec les variables 
	- axe1a* : scores des individus sur le 1er axe, dans les 4 ACM (une par année) où les individus sont donc actifs à chaque fois.
	- idem pour les autres axes
	- a98 axe* : scores de l'ACM initiale, où les individus son tactifs en 1998 puis supplémentaires les années suivantes.
	
	
	
*/
forvalues x = 1/3 {
	egen axe`x'a =  rowtotal(Ca*axe`x')  /* treats missing as 0 */
	replace axe`x'a = - axe`x'a
}


keep proj_gest_ntt a axe1a axe2a axe3a a98*

reshape wide axe* a98* , i(proj_gest_ntt) j(a)



* Corrélation entre les positions des individus dans l'ACM réalisée en 1998, 
*et la position qu'ils auraient eu si on avait fait une nouvelle ACM chaque année


* les vars a98* contiennent les positions des individus dans l'ACM de 1998 : individus actifs en 1998 et supplémentaires ensuite.

cap erase "$res/check09_MCA_correlations-axes.csv"
tempname myfile
file open `myfile' using "$res/check09_MCA_correlations-axes.csv", write
file write  `myfile' _n "Axis; Year; Coeff"
forvalues i = 1/3 { 
	foreach an of numlist 1998 2004 2009 2014 { 
	corr  a98axe`i'`an' axe`i'a`an'
	local coefcor =  r(rho) 
	file write `myfile' _n " `i' ; `an'  ;  `coefcor' "
	}
}
file close `myfile'

exit

