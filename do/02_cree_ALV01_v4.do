********************************************************************************
*	program:	cree_ALV01_v4.do
*	project:	ALV
*	author:		CD
*	date: 		20 Mai 2015
*   Adapatation du fichier cree_RL01_v4.do de MP par CD pour créer le fichier cree_ALV01_v4.do le 20/05/2015
/*	task:
v1 : prépare une première base "complète" ac infos individu, trajectoire et légumes
v2 : ajoute  info sur sortie emploi avant retraite gpso, recodée à part. ça marche pas
v3 : ajoute infos du fichier sur invalidité et fait les recodes ensuite
v4: corrige yr ((et crée yr_recup, en tenant compte de la récupération des congés.))
*/
local tag "cree_ALV01_v4.do cd 20mai2015"
********************************************************************************
set more off
// 1
*avant, ici je créais les morceaux à merger. je fais ça dans l'étape 1 du  dofile
	// 1 cree_individu01complet

//	2
//	merge
use "$cree\individu01.dta", clear
merge 1:1 proj_gest_ntt using "$temp\t_invalidite_v1"
drop _merge
merge 1:m proj_gest_ntt aqannee using "$cree\trajectoire01"
drop _merge
merge 1:1 proj_gest_ntt aqannee using "$cree\alimentation01"
drop _merge


merge 1:1 proj_gest_ntt aqannee using "$temp\t_codepostal.dta"
drop _merge
/*  MP le 2019-04-08 : ces morceaux n'existent pas encore.
merge 1:1 proj_gest_ntt aqannee using "$temp\t_actsoc.dta"
drop _merge

*/

//	3
//	organiser le fichier
drop aq_gener_sitfam aq_gener_nper aq_sitsoc_revenu ///
	

*boucher les trous 
* = recopier sur toutes les lignes les valeurs qui sont sur ligne 1 de cq indv
*ds annais-sortiedate jage
bys	proj_gest_ntt (aqannee): carryforward annais  invdate invaliditedate invlmmp  ///
    diplom        revenu_02   dejdom89 ///
    proper        gf1_mob       jpcs datnais       conj_pro_89   revpc_89 ///
	jage          gf1_35        sortieraison neap45 fumeur89 alcool89 nbhab_92   revpc_02  ///
    gf1_entree    pcs_entree    sortiedate femme revenu_89 proouv   jgf1  ///
	pcs_35   ev_retconjoint jagecat statutpere statutcjtbl ///
	, replace

	//	4
//	donner des noms potables aux var pour le traitement longitudinal
gen double i=proj_gest_ntt 
gen a=aqannee
label var i "Identifiant"
label var a "Année AQ"
note i: copie de proj_gest_ntt  / `tag'
note a: copie de aqannee / `tag'

gen retannee=year(retdate)
label var retannee "Année de retraite (GPSO)"
note retannee: `tag'

*premier aq APRES retraite OFFICIELLE(sera t=0)
gen  b=date(aqdate, "DMYhm")
/*replace b=MDY(1,1,aqannee) if aqdate==.*/
format b %d
replace b=aqannee if b==.


gen oh2= aqannee==retannee & b<retdate
bys i (a) : egen oh3=sum(oh2)
gen yr=aqannee-retannee
replace yr=aqannee-retannee-1 if oh3==1

label var yr "Années depuis retraite GPSO"
note yr : = AQ qui suit la retraite. si aq 26 janvier et retraite ///
	31 janvier : yr=-1. si pas de réponse à l'AQ, date fictive au 01/01 de l'année. ///
	nouveau calcul le 10 fév 2014. yr=aqannee-retannee, sauf si la date ///
	corrigée de l'AQ de l'année de la retraite (1/1/aqannee si date manquante) ///
	est antérieure à la date de retraite. cad l'AQ de l'année de la retraite est ///
	yr=0 sauf si AQ de l'année de retraite est antérieur à la retraite 	///
  	(ou manquant). `tag'
drop oh2 oh3

/*	date retraite corrigée des congés récupérés avant retraite
merge m:1 i	using "$temp/t_conge"
drop _merge

gen recup_jours=0
replace recup_jours=30 if recup==1	//si "moins de 2 mois" : 30 jours
replace recup_jours=60 if recup==2	// si 2-6 mois : le min, 60 jours
replace recup_jours=183 if recup==3	// si au moins 6 mois : le min : 6*30.5
label var recup_jours "récuparation congés avant retraite en jours"

gen retdate_recup=retdate - recup_jours
format retdate_recup %d
label var retdate_recup "Date retraite ac récup congés"

gen retannee_recup=year(retdate_recup)

gen oh2= aqannee==retannee_recup & b<retdate_recup
bys i (a) : egen oh3=sum(oh2)
gen yr_recup=aqannee-retannee_recup
replace yr_recup=aqannee-retannee_recup-1 if oh3==1
replace yr_recup=. if recup==.
drop oh2 oh3
label var yr_recup "temps % retraite ac récup congés"
note yr_recup : attention plein de NR sur la récupération des congés et leur durée. ///
	voir var recup. je déduis les jours de récupérations de congés de la date ///
	officielle de retraite pour situer la date effective de sortie d'emploi.	///	
	`tag'
*/

* invalidité, maladie pro et longue maladie
/* je n'ai pas assez d'info pour dater précisément la sortie d'emploi
je crée donc une indicatrice du fait d'avoir été en lm, inv ou maladie professionnelle
invlmmp actuellement n'existe pas si la pers n'a pas été en invalidité. je remplace les . par des 0*/
recode invlmmp (.=0)
note invlmmp: recodé dans `tag'

//	6
//	nettoyer, sauver
drop b 
note drop _dta 
note _dta: merge de individu01, legume01 et trajectoire01. Le fichier contient ///
	maintenant une ligne par indv et par année jusqu'à décès/sortie. / `tag'

order i a yr /*yr_recup*/ proj_gest_ntt aqannee aqrep aqdate retannee retdate invlmmp invdate invaliditedate  ///
	neap45 
compress 
sort i a
save "$cree\ALV01", replace
