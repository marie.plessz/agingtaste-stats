/*
Author : Marie Plessz
Project : AgingTaste
AgingTaste is a spin-off of the work on Gazel 
published in French in Gerontologie & société.

Start date : 25/06/2021.

Task : 
This file launches all the data management files for the project AgingTaste
The project uses the program mcacontrib.ado : 
you can run the file before starting, or you can store it in the ado folder 
of your computer (https://www.stata.com/support/faqs/programming/personal-ado-directory/).
mcacontrib is a helper to the stata mca command. it computes variable contribution to the
dimensions of the mca as in SPAD, SAS or R package FactoMiner.
*/


set more off
do "$do\01_cree_individu01complet_v2"
do "$do\02_cree_ALV01_v4"
do "$do\03_cree_ALV02recode_v5"
do "$do\05_cree_base_v1"
do "$do\06_cree_basewide_v2.do"
do "$do\07_cree_recodacm_v1"
do "$do\08_cree_AGT_inclus"
do "$do\09_cree_AGT_axes"
do "$do\10_AGT_modeles"
do "$do\11_AGT_statsdescs"

exit 

/*//	editer le codebook des data
cap log close
use "C:\Users\c_dion\Documents\Projet\ALV\Stata\data\cree\RL03inclus", clear
set more off
log using "$res/codebook_RL03", text replace
codebook, all note
log close

