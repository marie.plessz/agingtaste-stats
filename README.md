# AgingTaste-stats

## Context

Paper on aging and changes in tastes. intended for journal _Consumption and Society_.

Main empirical question : How do taste evolve over age? do differences accross educational groups reduce or increase?

Authored with Séverine Gojard. with contributions from Charlotte E Dion.

I Build on data preparation files that I have used in several occasions for Gazel, so a lot of recoding is not actually used here.

Software : Stata15.

## Data

Data : [Gazel cohort study](https://www.gazel.inserm.fr/fr/accueil), data extracted february 2015.  

Data are not publicly available because they contain sensitive personal information (health). They should be requested from the producers (on the website above).

## Setup

Before starting :

* Create the following folders in `AgingTaste-stats/` :

```
    data /

        source /    ==> for original datafiles

        cree /      ==> for datafiles created
        
        temp /      ==> for datafiles only used at a specific stage
```

* Run  `mcacontrib.ado` everytime you run the project, or add it to your `ado/personal` folder.
* In file `00_AGT_debut.do`, change the macro `$projet` so that it points to your project folder. Run this file everytime you start working on the project. 

## History

* v0 : code for the paper published in _Gérontologie et société_ in French, with no discussion on the ageing-education interaction and of the living arrangements.
* v1.0 : Version submitted, September 2021
* V2.0 : Revision 1,submitted March 2022 
